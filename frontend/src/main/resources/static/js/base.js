$(document).ready(function() {
    $.each($(".eventDescription"), function () {
        if ($(this).text().length > 80) {
            $(this).text($(this).text().substring(0, 80) + " ...");
        }
    });
});