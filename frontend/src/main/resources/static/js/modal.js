function showEventModal(id) {
    let a = document.getElementById('modalUrl');
    if (a != null) {
        a.href = "http://" + window.location.host + "/event/remove?id=" + id;
    }
}

function showTicketModal(id) {
    let a = document.getElementById('modalUrl');
    if (a != null) {
        a.href = "http://" + window.location.host + "/ticket/remove?id=" + id;
    }
}