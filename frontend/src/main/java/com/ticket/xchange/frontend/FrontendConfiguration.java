package com.ticket.xchange.frontend;

import com.ticket.xchange.backend.api.event.EventService;
import com.ticket.xchange.backend.api.ticket.TicketService;
import com.ticket.xchange.backend.api.transaction.TransactionService;
import com.ticket.xchange.backend.api.user.UserService;
import com.ticket.xchange.backend.api.wallet.WalletService;
import com.ticket.xchange.email.sender.api.EmailSender;
import com.ticket.xchange.util.JaxRsBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class FrontendConfiguration {

    @Value("${serviceAddress}")
    private String serverAddress;

    @Value("${emailSenderAddress}")
    private String emailSenderAddress;

    @Bean
    public UserService userService() { return JaxRsBean.createClient(UserService.class, serverAddress + "user/"); }

    @Bean
    public EventService eventService() { return JaxRsBean.createClient(EventService.class, serverAddress + "event/"); }

    @Bean
    public TicketService ticketService() { return JaxRsBean.createClient(TicketService.class, serverAddress + "ticket/"); }

    @Bean
    public WalletService walletService() { return JaxRsBean.createClient(WalletService.class, serverAddress + "wallet/"); }

    @Bean
    public TransactionService transactionService() { return JaxRsBean.createClient(TransactionService.class, serverAddress + "transaction/"); }

    @Bean
    public EmailSender emailSender() { return JaxRsBean.createClient(EmailSender.class, emailSenderAddress); }

}
