package com.ticket.xchange.frontend;

import com.ticket.xchange.backend.api.event.EventService;
import com.ticket.xchange.backend.api.ticket.TicketService;
import com.ticket.xchange.backend.api.transaction.TransactionService;
import com.ticket.xchange.backend.api.user.UserService;
import com.ticket.xchange.backend.api.wallet.WalletService;
import com.ticket.xchange.common.ModelStaticAttribute;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.event.EventEntry;
import com.ticket.xchange.common.ticket.TicketEntry;
import com.ticket.xchange.common.user.DashboardEntry;
import com.ticket.xchange.common.user.SessionEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.email.sender.api.EmailSender;
import com.ticket.xchange.util.SessionManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller("BaseController")
public class BaseController {

    private static SessionEntry sessionEntry;

    @Autowired @Qualifier(value = "userService") public UserService userService;
    @Autowired @Qualifier(value = "emailSender") public EmailSender emailSender;
    @Autowired @Qualifier(value = "eventService") public EventService eventService;
    @Autowired @Qualifier(value = "ticketService") public TicketService ticketService;
    @Autowired @Qualifier(value = "walletService") public WalletService walletService;
    @Autowired @Qualifier(value = "transactionService") public TransactionService transactionService;

    @Value("${frontendUrl}")
    public String frontendUrl;

    public void enrichModelAndView(ModelAndView model, String page) {
        model.setViewName("common/base");
        model.addObject("navbar","common/navbar");
//        model.addObject("footer","common/empty");
        model.addObject("footer", "common/footer");
        model.addObject("page", page);

        if (sessionEntry != null) {
            UserEntry userEntry = userService.getByEmail(sessionEntry.getUserEmail());
            if (userEntry != null) {
                if (UserType.ORGANIZER.equals(userEntry.getType())) {
                    model.addObject("isOrganizer", true);
                } else if (UserType.ADMINISTRATOR.equals(userEntry.getType())) {
                    model.addObject("isAdmin", true);
                }
                model.addObject("dashboardEntry", getDashboardEntry(userEntry));
            }
        }
    }

    public void enrichModelAndViewWithEmptyPage(ModelAndView model, String page) {
        model.setViewName("common/base");
        model.addObject("navbar","common/empty");
        model.addObject("footer", "common/empty");
        model.addObject("page", page);
    }

    public RedirectView getRedirectViewWithUrl(String url) {
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl(url);
        return redirectView;
    }

    public RedirectView getRedirectViewWithUrlAndStaticAttribute(String url, String attributeName, Object attributeValue) {
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl(url);
        redirectView.addStaticAttribute(attributeName, attributeValue);
        return redirectView;
    }

    public RedirectView getRedirectViewWithUrlAndStaticAttributes(String url, List<ModelStaticAttribute> modelStaticAttributes) {
        RedirectView redirectView = new RedirectView();
        redirectView.setContextRelative(true);
        redirectView.setUrl(url);

        for (ModelStaticAttribute modelAttribute : modelStaticAttributes) {
            redirectView.addStaticAttribute(modelAttribute.getAttributeName(), modelAttribute.getAttributeValue());
        }
        return redirectView;
    }

    public ModelAndView getErrorModel() {
        ModelAndView model = new ModelAndView();
        enrichModelAndViewWithEmptyPage(model, "error");
        return model;
    }

    public ModelAndView getForbiddenModel() {
        ModelAndView model = new ModelAndView();
        enrichModelAndViewWithEmptyPage(model, "forbidden");
        return model;
    }

    public boolean isAuthenticated(SessionEntry sessionEntry) {
        if (sessionEntry != null) {
            return SessionManagement.isSessionValid(sessionEntry.getSessionId());
        } return false;
    }

    public Boolean canUseActions(UserEntry currentSessionEntry, EventEntry eventEntry) {
        if (currentSessionEntry != null && eventEntry != null) {
            if (UserType.ADMINISTRATOR.equals(currentSessionEntry.getType())) return true;
            if (UserType.ORGANIZER.equals(currentSessionEntry.getType())) {
                return (eventEntry.getUserEntry().getEmail().equals(currentSessionEntry.getEmail()));
            }
        }
        return false;
    }

    public Boolean canUseActions(UserEntry currentSessionEntry, TicketEntry ticketEntry) {
        if (currentSessionEntry != null && ticketEntry != null) {
            if (UserType.ADMINISTRATOR.equals(currentSessionEntry.getType())) return true;
            return (ticketEntry.getUserEntry().getEmail().equals(currentSessionEntry.getEmail()));
        }
        return false;
    }

    public Boolean canUseActions(UserEntry currentSessionEntry) {
        if (currentSessionEntry != null) {
            return (UserType.ADMINISTRATOR.equals(currentSessionEntry.getType()) || UserType.ORGANIZER.equals(currentSessionEntry.getType()));
        }
        return false;
    }

    public Boolean canUseAdminActions(UserEntry currentSessionEntry) {
        if (currentSessionEntry != null) {
            return (UserType.ADMINISTRATOR.equals(currentSessionEntry.getType()));
        }
        return false;
    }

    public DashboardEntry getDashboardEntry(UserEntry currentSessionEntry) {
        if (currentSessionEntry != null) {
            DashboardEntry dashboardEntry = new DashboardEntry();
            dashboardEntry.setName(currentSessionEntry.getFirstName());

            if (currentSessionEntry.getWalletEntry() != null) {
                dashboardEntry.setBalance(currentSessionEntry.getWalletEntry().getBalance());
            } else {
                dashboardEntry.setBalance(0);
            }
            return dashboardEntry;
        } else return null;
    }

    public static SessionEntry getSessionEntry() {
        return sessionEntry;
    }

    public static void setSessionEntry(SessionEntry sessionEntry) {
        BaseController.sessionEntry = sessionEntry;
    }
}

