package com.ticket.xchange.frontend.controller.admin;

import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.ticket.xchange.common.enums.UserStatus;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.transaction.TransactionEntry;
import com.ticket.xchange.common.user.ModifyUserEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.frontend.BaseController;
import com.ticket.xchange.util.GeocodingUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller("AdminController")
public class AdminController extends BaseController {

    private static final String BASE_ADMIN_URL = "/admin";

    @GetMapping(BASE_ADMIN_URL + "/user/list")
    public ModelAndView userList(@RequestParam(value = "createSuccess", required = false) Boolean createSuccess,
                                  @RequestParam(value = "modifySuccess", required = false) Boolean modifySuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    if (!UserType.ADMINISTRATOR.equals(userEntry.getType())) {
                        return getForbiddenModel();
                    }
                } else return getErrorModel();

                ModelAndView model = new ModelAndView();

                if (createSuccess != null) {
                    model.addObject("createSuccess", createSuccess);
                }
                if (modifySuccess != null) {
                    model.addObject("modifySuccess", modifySuccess);
                }

                List<UserEntry> entryList = userService.getAll();
                if (!CollectionUtils.isEmpty(entryList)) {
                    model.addObject("userList", entryList);
                }

                enrichModelAndView(model, "content/admin/userList");
                return model;
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_ADMIN_URL + "/user/edit")
    public ModelAndView userModify(@RequestParam(value = "id", required = true) Long id,
                                    @RequestParam(value = "modifyUnsuccess", required = false) Boolean modifyUnsuccess,
                                    @RequestParam(value = "wrongLocation", required = false) Boolean wrongLocation,
                                    @RequestParam(value = "sameUser", required = false) Boolean sameUser){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (id != null) {
                    UserEntry userEntry = userService.getById(id);
                    if (userEntry != null) {
                        ModelAndView model = new ModelAndView();

                        if (modifyUnsuccess != null) {
                            model.addObject("modifyUnsuccess", modifyUnsuccess);
                        }
                        if (sameUser != null) {
                            model.addObject("sameUser", sameUser);
                        }
                        if (wrongLocation != null) {
                            model.addObject("wrongLocation", wrongLocation);
                        }

                        model.addObject("userEntry", userEntry);
                        model.addObject("modifyUserEntry", new ModifyUserEntry());
                        enrichModelAndView(model, "content/admin/userModify");
                        return model;
                    }
                }
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_ADMIN_URL + "/user/edit")
    public ModelAndView userModify(@ModelAttribute ModifyUserEntry modifyUserEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (modifyUserEntry != null) {
                    UserEntry existsEntry = userService.getById(modifyUserEntry.getId());
                    UserEntry toSaveEntry = new UserEntry();

                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (canUseAdminActions(userEntry)) {
                        if (userEntry == null || existsEntry == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_ADMIN_URL + "/user/edit?id=" + modifyUserEntry.getId()).
                                    addObject("modifyUnsuccess", true);
                        }
                        if (modifyUserEntry.getId().equals(userEntry.getId())) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_ADMIN_URL + "/user/edit?id=" + modifyUserEntry.getId()).
                                    addObject("sameUser", true);
                        }

                        JOpenCageLatLng latLng = GeocodingUtils.getLatitudeAngLongitudeFromAddress(modifyUserEntry.getCity());
                        if (latLng == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_ADMIN_URL + "/user/edit?id=" + modifyUserEntry.getId()).
                                    addObject("wrongLocation", true);
                        }

                        toSaveEntry.setId(modifyUserEntry.getId());
                        toSaveEntry.setEmail(modifyUserEntry.getEmail());
                        toSaveEntry.setPassword(existsEntry.getPassword());
                        toSaveEntry.setActivated(existsEntry.getActivated());
                        toSaveEntry.setToken(existsEntry.getToken());
                        toSaveEntry.setFirstName(modifyUserEntry.getFirstName());
                        toSaveEntry.setLastName(modifyUserEntry.getLastName());
                        toSaveEntry.setCity(modifyUserEntry.getCity());
                        toSaveEntry.setWalletEntry(existsEntry.getWalletEntry());

                        if (modifyUserEntry.getNotifications() == null) toSaveEntry.setNotifications(false);
                        else toSaveEntry.setNotifications(modifyUserEntry.getNotifications());

                        toSaveEntry.setType(UserType.valueOf(modifyUserEntry.getType()));
                        toSaveEntry.setRegistrationDate(existsEntry.getRegistrationDate());
                        toSaveEntry.setStatus(UserStatus.valueOf(modifyUserEntry.getStatus()));
                        toSaveEntry.setSessionId(existsEntry.getSessionId());

                        if (userService.modify(toSaveEntry) != null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_ADMIN_URL + "/user/list")
                                    .addObject("modifySuccess", true);
                        }
                        return new ModelAndView("redirect:" + frontendUrl + BASE_ADMIN_URL + "/user/edit?id=" + modifyUserEntry.getId()).
                                addObject("modifyUnsuccess", true);
                    }
                    return getForbiddenModel();
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_ADMIN_URL + "/transaction/list")
    public ModelAndView transactionList(){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();
                List<TransactionEntry> entryList = transactionService.getAll();
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());

                if (userEntry != null) {

                    if (!CollectionUtils.isEmpty(entryList)) {
                        model.addObject("transactionList", entryList);
                    }

                }
                enrichModelAndView(model, "content/transaction/transactionList");
                return model;
            }
        }
        return getErrorModel();
    }
}
