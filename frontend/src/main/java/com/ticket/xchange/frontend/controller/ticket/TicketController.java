package com.ticket.xchange.frontend.controller.ticket;

import com.ticket.xchange.common.enums.TicketStatus;
import com.ticket.xchange.common.enums.TransactionType;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.event.EventEntry;
import com.ticket.xchange.common.ticket.CreateTicketEntry;
import com.ticket.xchange.common.ticket.ModifyTicketEntry;
import com.ticket.xchange.common.ticket.TicketEntry;
import com.ticket.xchange.common.transaction.TransactionEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.common.wallet.WalletEntry;
import com.ticket.xchange.frontend.BaseController;
import com.ticket.xchange.util.DateTimeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.Instant;
import java.util.List;

@Controller("TicketController")
public class TicketController extends BaseController {

    private static final String BASE_TICKET_URL = "/ticket";

    @GetMapping(BASE_TICKET_URL)
    public ModelAndView ticketInfo(@RequestParam(value = "id", required = true) Long id) {
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (id != null) {
                    TicketEntry ticketEntry = ticketService.getById(id);
                    ModelAndView model = new ModelAndView();
                    if (ticketEntry != null) {
                        model.addObject("ticketEntry", ticketEntry);
                    }

                    enrichModelAndView(model, "content/ticket/ticketInfo");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_TICKET_URL + "/list")
    public ModelAndView ticketList(@RequestParam(value = "eventId", required = false) Long eventId,
                                  @RequestParam(value = "removeUnsuccess", required = false) Boolean removeUnsuccess,
                                  @RequestParam(value = "createSuccess", required = false) Boolean createSuccess,
                                  @RequestParam(value = "modifySuccess", required = false) Boolean modifySuccess,
                                  @RequestParam(value = "buyUnsuccess", required = false) Boolean buyUnsuccess,
                                  @RequestParam(value = "buySuccess", required = false) Boolean buySuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();
                List<TicketEntry> entryList;
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());

                if (userEntry != null) {
                    if (removeUnsuccess != null) {
                        model.addObject("removeUnsuccess", removeUnsuccess);
                    }
                    if (createSuccess != null) {
                        model.addObject("createSuccess", createSuccess);
                    }
                    if (modifySuccess != null) {
                        model.addObject("modifySuccess", modifySuccess);
                    }
                    if (buyUnsuccess != null) {
                        model.addObject("buyUnsuccess", buyUnsuccess);
                    }
                    if (buySuccess != null) {
                        model.addObject("buySuccess", buySuccess);
                    }

                    if (canUseAdminActions(userEntry)) {
                        entryList = ticketService.getAll();
                    } else {
                        if (eventId != null) {
                            entryList = ticketService.listAvaialbleByEventId(eventId, TicketStatus.SALE);
                        } else {
                            entryList = ticketService.listByStatus(TicketStatus.SALE);
                        }
                    }

                    if (!CollectionUtils.isEmpty(entryList)) {
                        model.addObject("ticketList", entryList);
                    }

                    if (UserType.ADMINISTRATOR.equals(userEntry.getType()) || UserType.ORGANIZER.equals(userEntry.getType())) {
                        model.addObject("hasPermission", true);
                    }
                    enrichModelAndView(model, "content/ticket/ticketList");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_TICKET_URL + "/my")
    public ModelAndView myTicketList(){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();
                List<TicketEntry> entryList;

                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    entryList = ticketService.listByUserId(userEntry.getId());

                    if (!CollectionUtils.isEmpty(entryList)) {
                        model.addObject("ticketList", entryList);
                    }
                }
                enrichModelAndView(model, "content/ticket/myTicketList");
                return model;
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_TICKET_URL + "/remove")
    public ModelAndView ticketRemove(@RequestParam(value = "id", required = true) Long id){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    TicketEntry ticketEntry = ticketService.getById(id);

                    if (ticketEntry == null) {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list").
                                addObject("removeUnsuccess", true);
                    }

                    if (canUseActions(userEntry, ticketEntry)) {
                        ticketEntry.setStatus(TicketStatus.DELETED);
                        if (ticketService.modify(ticketEntry) != null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list");
                        }
                    } else {
                        return getForbiddenModel();
                    }
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_TICKET_URL + "/create")
    public ModelAndView ticketCreate(@RequestParam(value = "createUnsuccess", required = false) Boolean createUnsuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();

                if (createUnsuccess != null) {
                    model.addObject("createUnsuccess", createUnsuccess);
                }

                List<EventEntry> entryList = eventService.getAll();
                if (!CollectionUtils.isEmpty(entryList)) {
                    model.addObject("eventList", entryList);
                }

                model.addObject("createTicketEntry", new CreateTicketEntry());
                enrichModelAndView(model, "content/ticket/ticketAdd");
                return model;
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_TICKET_URL + "/create")
    public ModelAndView ticketCreate(@ModelAttribute CreateTicketEntry createTicketEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (createTicketEntry != null) {
                    TicketEntry ticketEntry = new TicketEntry();
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    EventEntry eventEntry = eventService.getById(createTicketEntry.getEventId());

                    if (userEntry == null || eventEntry == null) {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/create").
                                addObject("createUnsuccess", true);
                    }

                    ticketEntry.setUserEntry(userEntry);
                    ticketEntry.setEventEntry(eventEntry);
                    ticketEntry.setName(createTicketEntry.getName());
                    ticketEntry.setPrice(createTicketEntry.getPrice());
                    ticketEntry.setValidity(DateTimeUtils.getInstantFromLocalDateTime(DateTimeUtils.getLocalDateTimeFromString(createTicketEntry.getValidity(), "yyyy.MM.dd. HH:mm")));
                    ticketEntry.setDescription(createTicketEntry.getDescription());
                    ticketEntry.setStatus(TicketStatus.SALE);
                    ticketEntry.setImageUrl(createTicketEntry.getImageUrl());

                    if (ticketService.create(ticketEntry) != null) {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list")
                                .addObject("createSuccess", true);
                    }
                    return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/create").
                            addObject("createUnsuccess", true);
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_TICKET_URL + "/edit")
    public ModelAndView ticketModify(@RequestParam(value = "id", required = true) Long id,
                                    @RequestParam(value = "modifyUnsuccess", required = false) Boolean modifyUnsuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (id != null) {
                    TicketEntry ticketEntry = ticketService.getById(id);
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (ticketEntry != null || userEntry != null) {
                        if (canUseActions(userEntry, ticketEntry)) {
                            ModelAndView model = new ModelAndView();

                            if (modifyUnsuccess != null) {
                                model.addObject("modifyUnsuccess", modifyUnsuccess);
                            }

                            List<EventEntry> entryList = eventService.getAll();
                            if (!CollectionUtils.isEmpty(entryList)) {
                                model.addObject("eventList", entryList);
                            }

                            model.addObject("ticketEntry", ticketEntry);
                            model.addObject("modifyTicketEntry", new ModifyTicketEntry());
                            enrichModelAndView(model, "content/ticket/ticketModify");
                            return model;
                        } else return getForbiddenModel();
                    }
                }
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_TICKET_URL + "/edit")
    public ModelAndView ticketModify(@ModelAttribute ModifyTicketEntry modifyTicketEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (modifyTicketEntry != null) {
                    TicketEntry ticketEntry = new TicketEntry();

                    TicketEntry existsTicketEntry = ticketService.getById(modifyTicketEntry.getId());
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    EventEntry eventEntry = eventService.getById(modifyTicketEntry.getEventId());

                    if (existsTicketEntry == null) return getForbiddenModel();

                    if (canUseActions(userEntry, existsTicketEntry)) {
                        if (userEntry == null || eventEntry == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/edit?id=" + modifyTicketEntry.getId()).
                                    addObject("modifyUnsuccess", true);
                        }

                        ticketEntry.setId(modifyTicketEntry.getId());
                        ticketEntry.setUserEntry(existsTicketEntry.getUserEntry());
                        ticketEntry.setEventEntry(eventEntry);
                        ticketEntry.setName(modifyTicketEntry.getName());
                        ticketEntry.setPrice(modifyTicketEntry.getPrice());
                        ticketEntry.setValidity(DateTimeUtils.getInstantFromLocalDateTime(DateTimeUtils.getLocalDateTimeFromString(modifyTicketEntry.getValidity(), "yyyy.MM.dd. HH:mm")));
                        ticketEntry.setDescription(modifyTicketEntry.getDescription());
                        ticketEntry.setStatus(existsTicketEntry.getStatus());
                        ticketEntry.setImageUrl(modifyTicketEntry.getImageUrl());

                        System.out.println(modifyTicketEntry);
                        System.out.println(ticketEntry);

                        if (ticketService.modify(ticketEntry) != null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list")
                                    .addObject("modifySuccess", true);
                        }
                        return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/edit?id=" + modifyTicketEntry.getId()).
                                addObject("modifyUnsuccess", true);
                    }
                    return getForbiddenModel();
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_TICKET_URL + "/buy")
    public ModelAndView ticketBuy(@RequestParam(value = "id", required = true) Long id){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    WalletEntry currentUserWalletEntry = userEntry.getWalletEntry();
                    TicketEntry ticketEntry = ticketService.getById(id);
                    UserEntry ticketSeller = ticketEntry.getUserEntry();

                    if (ticketEntry == null || currentUserWalletEntry == null || ticketSeller == null) {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list").
                                addObject("buyUnsuccess", true);
                    }

                    if (!ticketEntry.getUserEntry().getId().equals(userEntry.getId())) {
                        WalletEntry ticketUserWalletEntry = ticketEntry.getUserEntry().getWalletEntry();
                        if (currentUserWalletEntry.getBalance() >= ticketEntry.getPrice()) {
                            ticketEntry.setUserEntry(userEntry);
                            ticketEntry.setStatus(TicketStatus.BOUGHT);

                            currentUserWalletEntry.setBalance(currentUserWalletEntry.getBalance() - ticketEntry.getPrice());
                            ticketUserWalletEntry.setBalance(ticketUserWalletEntry.getBalance() + ticketEntry.getPrice());

                            walletService.modify(currentUserWalletEntry);
                            walletService.modify(ticketUserWalletEntry);

                            TransactionEntry transactionBuy = new TransactionEntry();
                            transactionBuy.setBuyerEntry(userEntry);
                            transactionBuy.setSellerEntry(ticketSeller);
                            transactionBuy.setEventEntry(ticketEntry.getEventEntry());
                            transactionBuy.setType(TransactionType.BUYING);
                            transactionBuy.setDate(Instant.now());
                            transactionBuy.setTicketEntry(ticketEntry);
                            transactionService.create(transactionBuy);

                            TransactionEntry transactionSell = new TransactionEntry();
                            transactionSell.setBuyerEntry(ticketSeller);
                            transactionSell.setSellerEntry(userEntry);
                            transactionSell.setEventEntry(ticketEntry.getEventEntry());
                            transactionSell.setType(TransactionType.SALE);
                            transactionSell.setDate(Instant.now());
                            transactionSell.setTicketEntry(ticketEntry);
                            transactionService.create(transactionSell);

                            emailSender.ticketBuy("hu", ticketSeller.getEmail().trim(), ticketSeller.getFirstName().trim(), ticketEntry.getName(), ticketEntry.getPrice());

                            if (ticketService.modify(ticketEntry) != null) {
                                return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list").
                                        addObject("buySuccess", true);
                            }
                        } else {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list").
                                    addObject("buyUnsuccess", true);
                        }
                    } else {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_TICKET_URL + "/list").
                                addObject("buyUnsuccess", true);
                    }
                }
            }
        }
        return getErrorModel();
    }
}
