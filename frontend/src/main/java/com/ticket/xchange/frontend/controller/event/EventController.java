package com.ticket.xchange.frontend.controller.event;

import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.event.CreateEventEntry;
import com.ticket.xchange.common.event.EventEntry;
import com.ticket.xchange.common.event.EventEntryDistance;
import com.ticket.xchange.common.event.ModifyEventEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.frontend.BaseController;
import com.ticket.xchange.util.DateTimeUtils;
import com.ticket.xchange.util.GeocodingUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller("EventController")
public class EventController extends BaseController {

    private static final String BASE_EVENT_URL = "/event";

    @GetMapping(BASE_EVENT_URL)
    public ModelAndView eventInfo(@RequestParam(value = "id", required = true) Long id) {
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (id != null) {
                    EventEntry eventEntry = eventService.getById(id);
                    ModelAndView model = new ModelAndView();
                    if (eventEntry != null) {
                        model.addObject("eventEntry", eventEntry);
                    }

                    enrichModelAndView(model, "content/event/eventInfo");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_EVENT_URL + "/list")
    public ModelAndView eventList(@RequestParam(value = "removeUnsuccess", required = false) Boolean removeUnsuccess,
                                  @RequestParam(value = "createSuccess", required = false) Boolean createSuccess,
                                  @RequestParam(value = "modifySuccess", required = false) Boolean modifySuccess,
                                  @RequestParam(value = "nearby", required = false) Boolean nearby){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();
                List<EventEntry> entryList = eventService.getAll();
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());

                if (userEntry != null) {
                    List<EventEntryDistance> events = mapToEventEntryWithDistance(entryList, userEntry);

                    if (removeUnsuccess != null) {
                        model.addObject("removeUnsuccess", removeUnsuccess);
                    }
                    if (createSuccess != null) {
                        model.addObject("createSuccess", createSuccess);
                    }
                    if (modifySuccess != null) {
                        model.addObject("modifySuccess", modifySuccess);
                    }

                    if (!CollectionUtils.isEmpty(events)) {
                        if (nearby != null) {
                            model.addObject("eventList", filterEventByDistance(events, 50.0));
                        } else {
                            model.addObject("eventList", events);
                        }
                    }

                    if (UserType.ADMINISTRATOR.equals(userEntry.getType()) || UserType.ORGANIZER.equals(userEntry.getType())) {
                        model.addObject("hasPermission", true);
                    }
                }
                enrichModelAndView(model, "content/event/eventList");
                return model;
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_EVENT_URL + "/remove")
    public ModelAndView eventRemove(@RequestParam(value = "id", required = true) Long id){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    EventEntry eventEntry = eventService.getById(id);

                    if (eventEntry == null) {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/list").
                                addObject("removeUnsuccess", true);
                    }

                    if (canUseActions(userEntry, eventEntry)) {
                        if (eventService.remove(eventEntry.getId())) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/list");
                        }
                    } else {
                        return getForbiddenModel();
                    }
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_EVENT_URL + "/create")
    public ModelAndView eventCreate(@RequestParam(value = "createUnsuccess", required = false) Boolean createUnsuccess,
                                    @RequestParam(value = "duplicateError", required = false) Boolean duplicateError,
                                    @RequestParam(value = "wrongLocation", required = false) Boolean wrongLocation){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();

                if (createUnsuccess != null) {
                    model.addObject("createUnsuccess", createUnsuccess);
                }
                if (duplicateError != null) {
                    model.addObject("duplicateError", duplicateError);
                }
                if (wrongLocation != null) {
                    model.addObject("wrongLocation", wrongLocation);
                }

                model.addObject("createEventEntry", new CreateEventEntry());
                enrichModelAndView(model, "content/event/eventAdd");
                return model;
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_EVENT_URL + "/create")
    public ModelAndView eventCreate(@ModelAttribute CreateEventEntry createEventEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (createEventEntry != null) {
                    EventEntry eventEntry = new EventEntry();
                    JOpenCageLatLng latLng = GeocodingUtils.getLatitudeAngLongitudeFromAddress(createEventEntry.getCity());
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (canUseActions(userEntry)) {
                        if (userEntry == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/create").
                                    addObject("createUnsuccess", true);
                        }
                        if (eventService.existsByName(createEventEntry.getName())) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/create").
                                    addObject("duplicateError", true);
                        }
                        if (latLng == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/create").
                                    addObject("wrongLocation", true);
                        }

                        eventEntry.setName(createEventEntry.getName());
                        eventEntry.setLongitude(latLng.getLng());
                        eventEntry.setLatitude(latLng.getLat());
                        eventEntry.setCity(createEventEntry.getCity());
                        eventEntry.setDate(DateTimeUtils.getInstantFromLocalDateTime(DateTimeUtils.getLocalDateTimeFromString(createEventEntry.getDate(), "yyyy.MM.dd. HH:mm")));
                        eventEntry.setDescription(createEventEntry.getDescription());
                        eventEntry.setUserEntry(userEntry);

                        if (eventService.create(eventEntry) != null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/list")
                                    .addObject("createSuccess", true);
                        }
                        return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/create").
                                addObject("createUnsuccess", true);
                    }
                    return getForbiddenModel();
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_EVENT_URL + "/edit")
    public ModelAndView eventModify(@RequestParam(value = "id", required = true) Long id,
                                    @RequestParam(value = "modifyUnsuccess", required = false) Boolean modifyUnsuccess,
                                    @RequestParam(value = "wrongLocation", required = false) Boolean wrongLocation){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (id != null) {
                    EventEntry eventEntry = eventService.getById(id);
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (eventEntry != null || userEntry != null) {
                        if (canUseActions(userEntry, eventEntry)) {
                            ModelAndView model = new ModelAndView();

                            if (modifyUnsuccess != null) {
                                model.addObject("modifyUnsuccess", modifyUnsuccess);
                            }
                            if (wrongLocation != null) {
                                model.addObject("wrongLocation", wrongLocation);
                            }

                            model.addObject("eventEntry", eventEntry);
                            model.addObject("modifyEventEntry", new ModifyEventEntry());
                            enrichModelAndView(model, "content/event/eventModify");
                            return model;
                        } else return getForbiddenModel();
                    }
                }
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_EVENT_URL + "/edit")
    public ModelAndView eventModify(@ModelAttribute ModifyEventEntry modifyEventEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (modifyEventEntry != null) {
                    EventEntry eventEntry = eventService.getById(modifyEventEntry.getId());
                    JOpenCageLatLng latLng = GeocodingUtils.getLatitudeAngLongitudeFromAddress(modifyEventEntry.getCity());
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (canUseActions(userEntry)) {
                        if (userEntry == null || eventEntry == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/edit?id=" + modifyEventEntry.getId()).
                                    addObject("modifyUnsuccess", true);
                        }
                        if (latLng == null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/edit?id=" + modifyEventEntry.getId()).
                                    addObject("wrongLocation", true);
                        }

                        eventEntry.setId(modifyEventEntry.getId());
                        eventEntry.setName(modifyEventEntry.getName());
                        eventEntry.setLongitude(latLng.getLng());
                        eventEntry.setLatitude(latLng.getLat());
                        eventEntry.setCity(modifyEventEntry.getCity());
                        eventEntry.setDate(DateTimeUtils.getInstantFromLocalDateTime(DateTimeUtils.getLocalDateTimeFromString(modifyEventEntry.getDate(), "yyyy.MM.dd. HH:mm")));
                        eventEntry.setDescription(modifyEventEntry.getDescription());

                        if (eventService.modify(eventEntry) != null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/list")
                                    .addObject("modifySuccess", true);
                        }
                        return new ModelAndView("redirect:" + frontendUrl + BASE_EVENT_URL + "/edit?id=" + modifyEventEntry.getId()).
                                addObject("modifyUnsuccess", true);
                    }
                    return getForbiddenModel();
                }
            }
        }
        return getErrorModel();
    }

    private List<EventEntryDistance> mapToEventEntryWithDistance(List<EventEntry> eventEntries, UserEntry userEntry) {
        if (CollectionUtils.isEmpty(eventEntries)) return new ArrayList<>();
        return eventEntries.stream().map(eventEntry -> getEventEntryWithDistance(eventEntry, userEntry)).collect(Collectors.toList());
    }

    private EventEntryDistance getEventEntryWithDistance(EventEntry eventEntry, UserEntry userEntry) {
        if (eventEntry == null) return null;
        EventEntryDistance entryDistance = new EventEntryDistance();
        entryDistance.setId(eventEntry.getId());
        entryDistance.setUserEntry(eventEntry.getUserEntry());
        entryDistance.setName(eventEntry.getName());
        entryDistance.setDate(eventEntry.getDate());
        entryDistance.setDescription(eventEntry.getDescription());
        entryDistance.setLatitude(eventEntry.getLatitude());
        entryDistance.setLongitude(eventEntry.getLongitude());
        entryDistance.setCity(eventEntry.getCity());
        entryDistance.setDistance(getDistance(eventEntry.getLatitude(), eventEntry.getLongitude(), userEntry.getCity()) / 1000.0);
        return entryDistance;
    }

    private List<EventEntryDistance> filterEventByDistance(List<EventEntryDistance> eventEntryDistance, Double distance){
        return eventEntryDistance.stream().filter(entry -> entry.getDistance() < distance).collect(Collectors.toList());
    }

    private Double getDistance(Double eventLat, Double eventLon, String userCity) {
        JOpenCageLatLng latLng = GeocodingUtils.getLatitudeAngLongitudeFromAddress(userCity);
        if (latLng != null)  {
            return GeocodingUtils.getDistance(eventLat, eventLon, latLng.getLat(), latLng.getLng());
        }
        return 0.0;
    }
}
