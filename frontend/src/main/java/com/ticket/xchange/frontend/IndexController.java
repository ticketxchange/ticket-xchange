package com.ticket.xchange.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller("IndexController")
public class IndexController extends BaseController {

    @GetMapping("/")
    public RedirectView indexPage(){
        return new RedirectView("/login");
    }

    @GetMapping("/landingPage")
    public ModelAndView landingPage(){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                ModelAndView model = new ModelAndView();
                model.addObject("sessionEntry", getSessionEntry());
                enrichModelAndView(model, "content/landing");
                return model;
            }
        }
        return getErrorModel();
    }

    @GetMapping("/detailed")
    public ModelAndView datailed(){
        ModelAndView model = new ModelAndView();
        enrichModelAndView(model, "content/ticket/detailed");
        return model;
    }

    @GetMapping("/ticketInfo")
    public ModelAndView ticketInfo(){
        ModelAndView model = new ModelAndView();
        enrichModelAndView(model, "content/ticket/ticketInfo");
        return model;
    }

}
