package com.ticket.xchange.frontend.controller.user;

import com.ticket.xchange.common.enums.UserStatus;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.user.*;
import com.ticket.xchange.common.ModelStaticAttribute;
import com.ticket.xchange.util.TokenGenerator;
import com.ticket.xchange.frontend.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;

@Controller("UserController")
public class UserController extends BaseController {

    @GetMapping("/login")
    public ModelAndView loginForm(@RequestParam(value = "wrongCredentials", required = false) Boolean wrongCredentials,
                                  @RequestParam(value = "registrationSuccess", required = false) Boolean registrationSuccess,
                                  @RequestParam(value = "notActivated", required = false) Boolean notActivated,
                                  @RequestParam(value = "activationSuccess", required = false) Boolean activationSuccess,
                                  @RequestParam(value = "invalidToken", required = false) Boolean invalidToken,
                                  @RequestParam(value = "forgottenPasswordSuccess", required = false) Boolean forgottenPasswordSuccess,
                                  @RequestParam(value = "newPasswordSuccess", required = false) Boolean newPasswordSuccess,
                                  @RequestParam(value = "userDeactivated", required = false) Boolean userDeactivated) {
        ModelAndView model = new ModelAndView();
        enrichModelAndViewWithEmptyPage(model, "content/login");
        model.addObject("loginEntry", new LoginEntry());

        if (wrongCredentials != null) {
            model.addObject("wrongCredentials", wrongCredentials);
        }
        if (registrationSuccess != null) {
            model.addObject("registrationSuccess", registrationSuccess);
        }
        if (notActivated != null) {
            model.addObject("notActivated", notActivated);
        }
        if (activationSuccess != null) {
            model.addObject("activationSuccess", activationSuccess);
        }
        if (invalidToken != null) {
            model.addObject("invalidToken", invalidToken);
        }
        if (forgottenPasswordSuccess != null) {
            model.addObject("forgottenPasswordSuccess", forgottenPasswordSuccess);
        }
        if (newPasswordSuccess != null) {
            model.addObject("newPasswordSuccess", newPasswordSuccess);
        }
        if (userDeactivated != null) {
            model.addObject("userDeactivated", userDeactivated);
        }
        return model;
    }

    @PostMapping("/login")
    public RedirectView login(@ModelAttribute LoginEntry loginEntry){
        if (loginEntry != null) {
            if (!StringUtils.isEmpty(loginEntry.getEmail()) && !StringUtils.isEmpty(loginEntry.getPassword())) {
                if (userService.isUserDeactivated(loginEntry.getEmail())) {
                    return getRedirectViewWithUrlAndStaticAttribute("/login", "userDeactivated", true);
                }
                UserEntry userEntry = userService.login(loginEntry);
                if (userEntry != null) {
                    if (UserStatus.ACTIVATED.equals(userEntry.getStatus())) {
                        setSessionEntry(getSessionEntry(userEntry));
                        return getRedirectViewWithUrl("/landingPage");
                    } else {
                        return getRedirectViewWithUrlAndStaticAttribute("/login", "notActivated", true);
                    }
                }
            }
        }
        return getRedirectViewWithUrlAndStaticAttribute("/login", "wrongCredentials", true);
    }

    @GetMapping("/activation")
    public RedirectView activation(@RequestParam(value = "token") String token){
        if (token != null) {
            UserEntry userEntry = userService.activation(token);
            if (userEntry != null) {
                return getRedirectViewWithUrlAndStaticAttribute("/login", "activationSuccess", true);
            }
        }
        return getRedirectViewWithUrlAndStaticAttribute("/login", "invalidToken", true);
    }

    @GetMapping("/registration")
    public ModelAndView registrationForm(@RequestParam(value = "usedEmail", required = false) Boolean usedEmail,
                                         @RequestParam(value = "wrongPassword", required = false) Boolean wrongPassword,
                                         @RequestParam(value = "passwordMismatch", required = false) Boolean passwordMismatch,
                                         @RequestParam(value = "registrationUnsuccess", required = false) Boolean registrationUnsuccess){
        ModelAndView model = new ModelAndView();
        enrichModelAndViewWithEmptyPage(model, "content/registration");
        model.addObject("registrationEntry", new RegistrationEntry());

        if (usedEmail != null) {
            model.addObject("usedEmail", usedEmail);
        }

        if (wrongPassword != null) {
            model.addObject("wrongPassword", wrongPassword);
        }

        if (passwordMismatch != null) {
            model.addObject("passwordMismatch", passwordMismatch);
        }

        if (registrationUnsuccess != null) {
            model.addObject("registrationUnsuccess", registrationUnsuccess);
        }
        return model;
    }

    @PostMapping("/registration")
    public RedirectView registration(@ModelAttribute RegistrationEntry registrationEntry){
        if (registrationEntry != null) {
            if (userService.isEmailUsed(registrationEntry.getEmail().trim())) { //Ha használt az E-mail
                return getRedirectViewWithUrlAndStaticAttribute("/registration", "usedEmail", true);
            }

            if (!registrationEntry.getPassword().trim().equals(registrationEntry.getPasswordConfirm().trim())) {
                return getRedirectViewWithUrlAndStaticAttribute("/registration", "passwordMismatch", true);
            }

            if (!userService.isPasswordGood(registrationEntry.getPassword())) {
                return getRedirectViewWithUrlAndStaticAttribute("/registration", "wrongPassword", true);
            }

            String token = TokenGenerator.getRandomToken();
            UserEntry toRegister = getToRegisterEntry(registrationEntry, token);
            UserEntry userEntry = userService.registration(toRegister);

            if (userEntry == null) {
                return getRedirectViewWithUrlAndStaticAttribute("/registration", "registrationUnsuccess", true);
            }

            emailSender.register("hu", registrationEntry.getEmail().trim(), registrationEntry.getFirstName().trim(), frontendUrl + "/activation?token=" + token);
        }

        return getRedirectViewWithUrlAndStaticAttribute("/login", "registrationSuccess", true);
    }

    @GetMapping("/forgottenPassword")
    public ModelAndView forgottenPasswordForm(@RequestParam(value = "forgottenPasswordUnsuccess", required = false) Boolean forgottenPasswordUnsuccess){
        ModelAndView model = new ModelAndView();
        enrichModelAndViewWithEmptyPage(model, "content/forgottenPassword");
        model.addObject("forgottenPasswordEntry", new ForgottenPasswordEntry());

        if (forgottenPasswordUnsuccess != null) {
            model.addObject("forgottenPasswordUnsuccess", forgottenPasswordUnsuccess);
        }

        return model;
    }

    @PostMapping("/forgottenPassword")
    public RedirectView sendForgottenPassword(@ModelAttribute ForgottenPasswordEntry forgottenPasswordEntry){
        if (forgottenPasswordEntry != null) {
            if (forgottenPasswordEntry.getEmail() != null) {
                String token = TokenGenerator.getRandomToken();
                if (userService.forgottenPassword(forgottenPasswordEntry.getEmail().trim(), token)) {
                    emailSender.forgottenPassword("hu", forgottenPasswordEntry.getEmail().trim(), "Felhasználó", frontendUrl + "/newPassword?token=" + token, "");
                    return getRedirectViewWithUrlAndStaticAttribute("/login", "forgottenPasswordSuccess", true);
                }
            }
        }
        return getRedirectViewWithUrlAndStaticAttribute("/forgottenPassword", "forgottenPasswordUnsuccess", true);
    }

    @GetMapping("/newPassword")
    public ModelAndView nwPasswordForm(@RequestParam(value = "token", required = false) String token,
                                       @RequestParam(value = "newPasswordUnsuccess", required = false) Boolean newPasswordUnsuccess,
                                       @RequestParam(value = "wrongPassword", required = false) Boolean wrongPassword,
                                       @RequestParam(value = "passwordMismatch", required = false) Boolean passwordMismatch){
        ModelAndView model = new ModelAndView();
        enrichModelAndViewWithEmptyPage(model, "content/newPassword");
        model.addObject("forgottenPasswordEntry", new ForgottenPasswordEntry("", "", "", token));

        if (newPasswordUnsuccess != null) {
            model.addObject("newPasswordUnsuccess", newPasswordUnsuccess);
        }

        if (wrongPassword != null) {
            model.addObject("wrongPassword", wrongPassword);
        }

        if (passwordMismatch != null) {
            model.addObject("passwordMismatch", passwordMismatch);
        }

        return model;
    }

    @PostMapping("/newPassword")
    public RedirectView newPassword(@ModelAttribute ForgottenPasswordEntry forgottenPasswordEntry){
        if (forgottenPasswordEntry != null) {
            if (!forgottenPasswordEntry.getPassword().trim().equals(forgottenPasswordEntry.getPasswordConfirm().trim())) {
                List<ModelStaticAttribute> attributeList = new ArrayList<>();
                attributeList.add(new ModelStaticAttribute("passwordMismatch", true));
                attributeList.add(new ModelStaticAttribute("token", forgottenPasswordEntry.getToken()));
                return getRedirectViewWithUrlAndStaticAttributes("/newPassword", attributeList);
            }

            if (!userService.isPasswordGood(forgottenPasswordEntry.getPassword())) {
                List<ModelStaticAttribute> attributeList = new ArrayList<>();
                attributeList.add(new ModelStaticAttribute("wrongPassword", true));
                attributeList.add(new ModelStaticAttribute("token", forgottenPasswordEntry.getToken()));
                return getRedirectViewWithUrlAndStaticAttributes("/newPassword", attributeList);
            }

            UserEntry userExists = userService.getByToken(forgottenPasswordEntry.getToken());
            if (userExists != null) {
                userExists.setPassword(forgottenPasswordEntry.getPassword());
                userExists.setToken("");
                userExists.setStatus(UserStatus.ACTIVATED);
            }

            UserEntry userEntry = userService.modify(userExists);

            if (userEntry == null) {
                List<ModelStaticAttribute> attributeList = new ArrayList<>();
                attributeList.add(new ModelStaticAttribute("newPasswordUnsuccess", true));
                attributeList.add(new ModelStaticAttribute("token", forgottenPasswordEntry.getToken()));
                return getRedirectViewWithUrlAndStaticAttributes("/newPassword", attributeList);
            }
        }

        return getRedirectViewWithUrlAndStaticAttribute("/login", "newPasswordSuccess", true);
    }

    @GetMapping("/logout")
    public RedirectView logout(){
        if (getSessionEntry() != null) {
            Boolean logoutSuccess = userService.logout(getSessionEntry().getUserEmail());
            if (logoutSuccess) {
                setSessionEntry(null);
                return new RedirectView("/");
            }
        }
        return new RedirectView("/");
    }

    private UserEntry getToRegisterEntry(RegistrationEntry registrationEntry, String token) {
        UserEntry toRegister = new UserEntry();
        toRegister.setEmail(registrationEntry.getEmail().trim());
        toRegister.setPassword(registrationEntry.getPassword().trim());
        toRegister.setActivated(false);
        toRegister.setToken(token);
        toRegister.setFirstName(registrationEntry.getFirstName().trim());
        toRegister.setLastName(registrationEntry.getLastName().trim());
        toRegister.setCity(registrationEntry.getCity().trim());
        toRegister.setNotifications(false);
        toRegister.setType(UserType.NORMAL_USER);
        toRegister.setStatus(UserStatus.NOT_ACTIVATED);
        return toRegister;
    }

    private SessionEntry getSessionEntry(UserEntry userEntry) {
        SessionEntry sessionEntry = new SessionEntry();
        sessionEntry.setUserEmail(userEntry.getEmail());
        sessionEntry.setSessionId(userEntry.getSessionId());
        return sessionEntry;
    }
}
