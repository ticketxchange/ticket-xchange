package com.ticket.xchange.frontend.controller.profile;

import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.ticket.xchange.common.user.ModifyProfileEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.frontend.BaseController;
import com.ticket.xchange.util.GeocodingUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller("ProfileController")
public class ProfileController extends BaseController {

    private static final String BASE_PROFILE_URL = "/profile";

    @GetMapping(BASE_PROFILE_URL)
    public ModelAndView profile(@RequestParam(value = "registrationSuccess", required = false) Boolean registrationSuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    ModelAndView model = new ModelAndView();
                    model.addObject("userEntry", userEntry);

                    if (registrationSuccess != null) {
                        model.addObject("registrationSuccess", registrationSuccess);
                    }

                    JOpenCageLatLng latLong = GeocodingUtils.getLatitudeAngLongitudeFromAddress(userEntry.getCity());
                    if (latLong != null) {
                        model.addObject("lat", latLong.getLat());
                        model.addObject("lon", latLong.getLng());
                    }

                    enrichModelAndView(model, "content/profile/profile");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_PROFILE_URL + "/edit")
    public ModelAndView editProfile(@RequestParam(value = "wrongPassword", required = false) Boolean wrongPassword,
                                    @RequestParam(value = "modifyProfileUnsuccess", required = false) Boolean modifyProfileUnsuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    ModelAndView model = new ModelAndView();

                    if (wrongPassword != null) {
                        model.addObject("wrongPassword", wrongPassword);
                    }

                    if (modifyProfileUnsuccess != null) {
                        model.addObject("modifyProfileUnsuccess", modifyProfileUnsuccess);
                    }

                    model.addObject("userEntry", userEntry);
                    model.addObject("modifyProfileEntry", new ModifyProfileEntry());
                    enrichModelAndView(model, "content/profile/editProfile");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_PROFILE_URL + "/edit")
    public ModelAndView modifyProfile(@ModelAttribute ModifyProfileEntry modifyProfileEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (modifyProfileEntry != null) {
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (userEntry != null) {
                        if (userEntry.getPassword().equals(modifyProfileEntry.getPassword())) {
                            userEntry.setFirstName(modifyProfileEntry.getFirstName());
                            userEntry.setLastName(modifyProfileEntry.getLastName());
                            userEntry.setCity(modifyProfileEntry.getCity());

                            if (modifyProfileEntry.getNotifications() != null) userEntry.setNotifications(modifyProfileEntry.getNotifications());
                            else userEntry.setNotifications(false);

                            if (userService.modify(userEntry) != null) {
                                return new ModelAndView("redirect:" + frontendUrl + BASE_PROFILE_URL).
                                        addObject("modifyProfileSuccess", true);
                            } else {
                                return new ModelAndView("redirect:" + frontendUrl + BASE_PROFILE_URL + "/edit").
                                        addObject("modifyProfileUnsuccess", true);
                            }
                        } else {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_PROFILE_URL + "/edit").
                                    addObject("wrongPassword", true);
                        }
                    }
                }
                return new ModelAndView("redirect:" + frontendUrl + BASE_PROFILE_URL + "/edit").
                        addObject("modifyProfileUnsuccess", true);
            }
        }
        return getErrorModel();
    }

}
