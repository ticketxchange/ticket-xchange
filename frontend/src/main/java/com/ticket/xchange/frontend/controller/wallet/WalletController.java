package com.ticket.xchange.frontend.controller.wallet;

import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.ticket.xchange.common.enums.TransactionType;
import com.ticket.xchange.common.transaction.TransactionEntry;
import com.ticket.xchange.common.user.ModifyProfileEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.common.wallet.ModifyWalletEntry;
import com.ticket.xchange.common.wallet.WalletEntry;
import com.ticket.xchange.frontend.BaseController;
import com.ticket.xchange.util.GeocodingUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.Instant;

@Controller("WalletController")
public class WalletController extends BaseController {

    private static final String BASE_WALLET_URL = "/wallet";

    @GetMapping(BASE_WALLET_URL)
    public ModelAndView walletInfo(@RequestParam(value = "modifySuccess", required = false) Boolean modifySuccess,
                                @RequestParam(value = "depositSuccess", required = false) Boolean depositSuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    ModelAndView model = new ModelAndView();
                    model.addObject("walletEntry", userEntry.getWalletEntry());

                    if (modifySuccess != null) {
                        model.addObject("modifySuccess", modifySuccess);
                    }

                    if (depositSuccess != null) {
                        model.addObject("depositSuccess", depositSuccess);
                    }

                    enrichModelAndView(model, "content/wallet/walletInfo");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_WALLET_URL + "/edit")
    public ModelAndView walletModify(@RequestParam(value = "modifyUnsuccess", required = false) Boolean modifyUnsuccess){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    ModelAndView model = new ModelAndView();
                    model.addObject("walletEntry", userEntry.getWalletEntry());
                    model.addObject("modifyWalletEntry", new ModifyWalletEntry());

                    if (modifyUnsuccess != null) {
                        model.addObject("modifyUnsuccess", modifyUnsuccess);
                    }

                    enrichModelAndView(model, "content/wallet/walletModify");
                    return model;
                }
            }
        }
        return getErrorModel();
    }

    @PostMapping(BASE_WALLET_URL + "/edit")
    public ModelAndView modifyProfile(@ModelAttribute ModifyWalletEntry modifyWalletEntry){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                if (modifyWalletEntry != null) {
                    UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                    if (userEntry != null) {
                        WalletEntry toSave = userEntry.getWalletEntry();

                        if (StringUtils.isEmpty(modifyWalletEntry.getCardNumber())) { toSave.setCardNumber(null); }
                        else { toSave.setCardNumber(modifyWalletEntry.getCardNumber()); }

                        if (StringUtils.isEmpty(modifyWalletEntry.getExpirationDate())) { toSave.setExpirationDate(null); }
                        else { toSave.setExpirationDate(modifyWalletEntry.getExpirationDate()); }

                        if (StringUtils.isEmpty(modifyWalletEntry.getSecurityCode())) { toSave.setSecurityCode(null); }
                        else { toSave.setSecurityCode(modifyWalletEntry.getSecurityCode()); }

                        if (walletService.modify(toSave) != null) {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_WALLET_URL).
                                    addObject("modifySuccess", true);
                        } else {
                            return new ModelAndView("redirect:" + frontendUrl + BASE_WALLET_URL + "/edit").
                                    addObject("modifyWalletUnsuccess", true);
                        }
                    }
                }
                return new ModelAndView("redirect:" + frontendUrl + BASE_WALLET_URL + "/edit").
                        addObject("modifyWalletUnsuccess", true);
            }
        }
        return getErrorModel();
    }

    @GetMapping(BASE_WALLET_URL + "/deposit")
    public ModelAndView walletDeposit(){
        if (getSessionEntry() != null) {
            if (isAuthenticated(getSessionEntry())) {
                UserEntry userEntry = userService.getByEmail(getSessionEntry().getUserEmail());
                if (userEntry != null) {
                    WalletEntry walletEntry = userEntry.getWalletEntry();
                    walletEntry.setBalance(walletEntry.getBalance() + 5000);

                    TransactionEntry transactionDeposit = new TransactionEntry();
                    transactionDeposit.setBuyerEntry(userEntry);
                    transactionDeposit.setSellerEntry(null);
                    transactionDeposit.setEventEntry(null);
                    transactionDeposit.setType(TransactionType.DEPOSIT);
                    transactionDeposit.setDate(Instant.now());
                    transactionDeposit.setTicketEntry(null);
                    transactionService.create(transactionDeposit);

                    if (walletService.modify(walletEntry) != null) {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_WALLET_URL).
                                addObject("depositSuccess", true);
                    } else {
                        return new ModelAndView("redirect:" + frontendUrl + BASE_WALLET_URL).
                                addObject("depositUnsuccess", true);
                    }
                }
            }
        }
        return getErrorModel();
    }
}
