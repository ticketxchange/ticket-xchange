package com.ticket.xchange.util;

public class TokenGenerator {

    private static final Integer TOKEN_LENGTH = 16;
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz0123456789";

    public static String getRandomToken() {
        StringBuilder sb = new StringBuilder(TOKEN_LENGTH);

        for (int i = 0; i < TOKEN_LENGTH; i++) {
            int index = (int)(CHARACTERS.length() * Math.random());
            sb.append(CHARACTERS.charAt(index));
        }

        return sb.toString();
    }

}
