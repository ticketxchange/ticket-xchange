package com.ticket.xchange.util;

public class PasswordPolicyChecker {

    private final String PATTERN = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}";
    private String passwordTrimmed;

    public PasswordPolicyChecker(String password) {
        this.passwordTrimmed = password.trim();
    }

    public boolean isPasswordGood() {
        if (passwordTrimmed == null) return false;
        return (passwordTrimmed.matches(PATTERN));
    }

}
