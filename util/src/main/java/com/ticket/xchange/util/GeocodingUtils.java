package com.ticket.xchange.util;

import com.byteowls.jopencage.JOpenCageGeocoder;
import com.byteowls.jopencage.model.JOpenCageForwardRequest;
import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.byteowls.jopencage.model.JOpenCageResponse;
import com.byteowls.jopencage.model.JOpenCageReverseRequest;
import net.sf.geographiclib.Geodesic;
import net.sf.geographiclib.GeodesicLine;
import net.sf.geographiclib.GeodesicMask;

public class GeocodingUtils {

    private static final String OPEN_CAGE_API_KEY = "d3b0c706ee5d483db517ca1440da7549";

    public static String getAddressFromLatitudeAndLongitude(Double latitude, Double longitude) {
        JOpenCageGeocoder jOpenCageGeocoder = new JOpenCageGeocoder(OPEN_CAGE_API_KEY);

        JOpenCageReverseRequest request = new JOpenCageReverseRequest(latitude, longitude);
        request.setLanguage("hu");
        request.setNoDedupe(true);
        request.setLimit(5);
        request.setNoAnnotations(true);
        request.setMinConfidence(3);

        JOpenCageResponse response = jOpenCageGeocoder.reverse(request);

        return response.getResults().get(0).getFormatted();
    }

    public static JOpenCageLatLng getLatitudeAngLongitudeFromAddress(String city) {
        JOpenCageGeocoder jOpenCageGeocoder = new JOpenCageGeocoder(OPEN_CAGE_API_KEY);
        JOpenCageForwardRequest request = new JOpenCageForwardRequest(city);
        request.setLanguage("hu");
        request.setPretty(true);

        JOpenCageResponse response = jOpenCageGeocoder.forward(request);
        return response.getFirstPosition();
    }

    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        GeodesicLine line = Geodesic.WGS84.InverseLine(lat1, lon1, lat2, lon2, GeodesicMask.DISTANCE_IN | GeodesicMask.LATITUDE | GeodesicMask.LONGITUDE);
        return line.Distance();
    }
}
