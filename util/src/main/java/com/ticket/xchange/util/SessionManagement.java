package com.ticket.xchange.util;

import org.apache.cxf.common.util.StringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class SessionManagement {

    private static final int SESSION_INVALID_TIME_MINUTES = 30;

    public static String getRandomSessionId() {
        String token = TokenGenerator.getRandomToken();

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter yearFormatter = DateTimeFormatter.ofPattern("yyyy");
        DateTimeFormatter monthFormatter = DateTimeFormatter.ofPattern("MM");
        DateTimeFormatter dayFormatter = DateTimeFormatter.ofPattern("dd");
        DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH");
        DateTimeFormatter minuteFormatter = DateTimeFormatter.ofPattern("mm");

        String yearString = now.format(yearFormatter);
        String monthString = now.format(monthFormatter);
        String dayString = now.format(dayFormatter);
        String hourString = now.format(hourFormatter);
        String minuteString = now.format(minuteFormatter);
        return token + yearString + monthString + dayString + hourString + minuteString;
    }

    public static boolean isSessionValid(String sessionId) {
        if (!StringUtils.isEmpty(sessionId) && sessionId.length() == 28) {
            LocalDateTime sessionDateTime = DateTimeUtils.getLocalDateTimeFromString(getLocalDateTimeStringFromSessionId(sessionId),"yyyyMMddHHmm");
            long diff = ChronoUnit.MINUTES.between(sessionDateTime, LocalDateTime.now());
            return (diff < SESSION_INVALID_TIME_MINUTES);
        } else return false;
    }

    private static String getLocalDateTimeStringFromSessionId(String sessionId) {
        return sessionId.substring(16);
    }

}
