package com.ticket.xchange.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;

public class JaxRsBean {
    private static ObjectMapper getObjectMapper(){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        SimpleModule module = new SimpleModule();
        return objectMapper;
    }

    public static <T> T createClient(Class<T> beanClass, String address) {
        JAXRSClientFactoryBean jaxrsClientFactoryBean = new JAXRSClientFactoryBean();
        jaxrsClientFactoryBean.setServiceClass(beanClass);
        jaxrsClientFactoryBean.setAddress(address);
        JacksonJsonProvider provider = new JacksonJsonProvider(getObjectMapper());
        jaxrsClientFactoryBean.setProvider(provider);
        return (T)jaxrsClientFactoryBean.create();
    }

    public static <T> Server createServer(Bus bus, T serverImplementation, String path) {
        JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
        endpoint.setBus(bus);
        endpoint.setServiceBean(serverImplementation);
        endpoint.setProvider(new JacksonJsonProvider(getObjectMapper()));
        endpoint.setAddress(path);
        return endpoint.create();
    }
}
