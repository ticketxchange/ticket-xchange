package com.ticket.xchange.email.sender.service;

import com.ticket.xchange.util.JaxRsBean;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

@Configuration
public class EmailSenderConfiguration {

    @Value("${smtp.starttls_enable}")
    private String starttlsEnable;

    @Value("${smtp.auth}")
    private String auth;

    @Value("${smtp.host}")
    private String host;

    @Value("${smtp.port}")
    private String port;

    @Value("${smtp.socketfactory_port}")
    private String socketfactoryPort;

    @Value("${smtp.user}")
    private String user;

    @Value("${smtp.password}")
    private String password;

    @Autowired
    private EmailSenderImpl emailSender;

    @Bean(name = "cxf")
    public SpringBus bus() {
        return new SpringBus();
    }

    @Bean
    public Server server() {
        return JaxRsBean.createServer(bus(), emailSender, "/");
    }

    @Bean
    public ServletRegistrationBean cxfServletRegistrationBean() {
        return new ServletRegistrationBean(new CXFServlet(), "/*");
    }

    @Bean(name = "emailSettings")
    Properties getEmailSettings() {
        Properties settings = new Properties();

        if(starttlsEnable != null) {
            settings.put("mail.smtp.starttls.enable", starttlsEnable);
        }

        if(auth != null) {
            settings.put("mail.smtp.auth", auth);
        }

        if(host != null) {
            settings.put("mail.smtp.host", host);
        }

        if(port != null) {
            settings.put("mail.smtp.port", port);
        }

        if(socketfactoryPort != null) {
            settings.put("mail.smtp.socketFactory.port", socketfactoryPort);
        }

        settings.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        settings.put("mail.smtp.socketFactory.fallback", "false");
        return settings;
    }

    @Bean(name = "passwordAuthentication")
    PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(
                user,
                password);
    }

    @Bean(name = "authenticator")
    javax.mail.Authenticator getAuthenticator(PasswordAuthentication passwordAuthentication) {
        return new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return passwordAuthentication;
            }
        };
    }

    @Bean(name = "emailSession")
    Session getEmailSession(Properties emailSettings, javax.mail.Authenticator authenticator) throws MessagingException {
        Session resultsession;
        resultsession = Session.getInstance(emailSettings, authenticator);

        if (resultsession == null) {
            throw new MessagingException("Email configuration error.");
        }
        resultsession.setDebug(false);
        return resultsession;
    }

}
