package com.ticket.xchange.email.sender.service;

import com.ticket.xchange.email.sender.api.EmailSender;
import org.apache.commons.text.StrSubstitutor;
import org.apache.cxf.helpers.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Component
public class EmailSenderImpl implements EmailSender {

    static final String EXCEPTION = "EmailSenderImplException";
    static final String RESOURCE_PATH_PREFIX = "/files/";
    static Map<String, String> contentCache;
    static Map<String, String> subjectCache;

    static final String TEMPL_LOCALE = "locale";
    static final String TEMPL_ADDRESS = "address";
    static final String TEMPL_USERNAME = "username";
    static final String TEMPL_URL = "url";
    static final String TEMPL_ENVIRONMENT = "environment";
    static final String POST_FAILED = "POST:register : Failed";
    static final String CHARSET_UTF_8 = "utf-8";
    static final String TEMPL_CONTETN = "text/html; charset=utf-8";

    @Autowired
    private Session session;

    @Value("${emailsenderaddr}")
    private String senderAddr;

    static final String TEMPL_SUF_CONTENT = "_CN";
    static final String TEMPL_SUF_SUBJECT = "_SB";

    static final String TEMPL_REGISTRATION = "registration";

    private Response registrationHelepr(Map<String, String> vm) {

        try {
            if (!haveTemplate(vm.get(TEMPL_LOCALE), TEMPL_REGISTRATION)) {
                addTemplateToCache(vm.get(TEMPL_LOCALE), TEMPL_REGISTRATION);
            }
        } catch (IOException e) {
            return Response.status(500).entity(POST_FAILED).build();
        }

        String content = getContent(vm.get(TEMPL_LOCALE), TEMPL_REGISTRATION, vm);
        String subject = getSubject(vm.get(TEMPL_LOCALE), TEMPL_REGISTRATION, vm);

        if (content == null || subject == null) {
            return Response.status(500).entity("POST:register : Template not found").build();
        }

        try {
            Message email = new MimeMessage(session);
            email.setFrom(new InternetAddress(senderAddr));
            email.setRecipient(Message.RecipientType.TO, new InternetAddress(vm.get(TEMPL_ADDRESS)));
            email.setSubject(MimeUtility.encodeText(subject, CHARSET_UTF_8, "B"));
            email.setContent(content, TEMPL_CONTETN);
            Transport.send(email);
        } catch (UnsupportedEncodingException | MessagingException e) {
            return Response.status(500).entity(POST_FAILED).build();
        }

        return Response.status(200).entity("POST:register : All good").build();
    }

    @Override
    public Response register(String locale, String address, String name, String url) {

        Map<String, String> vm = new HashMap<>();
        vm.put(TEMPL_LOCALE, locale);
        vm.put(TEMPL_ADDRESS, address);
        vm.put(TEMPL_USERNAME, name);
        vm.put(TEMPL_URL, url);
        vm.put(TEMPL_ENVIRONMENT, "dev");

        return registrationHelepr(vm);
    }

    public Response responseBuilder(Boolean result) {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        String msg;
        if (result) {
            msg = "POST:" + ste[ste.length - 1 - 1].getMethodName() + " : All good";
            /*Second '1' indicates the depth. If changed to '0', it will return the name of the running method, '1' returns the name of the calling method*/
            return Response.status(200).entity(msg).build();
        }
        msg = "POST:" + ste[ste.length - 1 - 1].getMethodName() + " : Failed";
        return Response.status(500).entity(msg).build();
    }

    @Override
    public Response forgottenPassword(String locale, String address, String name, String url, String saveUrl) {
        Boolean result = sendEmail("forgotten_password", locale, address, name, url, saveUrl);
        return responseBuilder(result);
    }

    @Override
    public Response nearbyEvent(String locale, String address, String name, String url, String saveUrl) {
        Boolean result = sendEmail("nearby_event", locale, address, name, url, saveUrl);
        return responseBuilder(result);
    }

    @Override
    public Response ticketBuy(String locale, String address, String name, String ticketName, Integer ticketPrice) {
        Map<String, String> vm = new HashMap<>();
        vm.put(TEMPL_LOCALE, locale);
        vm.put(TEMPL_ADDRESS, address);
        vm.put(TEMPL_USERNAME, name);
        vm.put("ticketName", ticketName);
        vm.put("ticketPrice", ticketPrice.toString());
        vm.put(TEMPL_ENVIRONMENT, "dev");

        try {
            if (!haveTemplate(locale, "ticket_buy")) {
                addTemplateToCache(locale, "ticket_buy");
            }

            String content = getContent(locale, "ticket_buy", vm);
            String subject = getSubject(locale, "ticket_buy", vm);

            Message email = new MimeMessage(session);
            email.setFrom(new InternetAddress(senderAddr));
            email.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
            email.setSubject(MimeUtility.encodeText(subject, CHARSET_UTF_8, "B"));
            email.setContent(content, TEMPL_CONTETN);
            Transport.send(email);
            return responseBuilder(Boolean.TRUE);
        } catch (IOException | MessagingException e) {
            return responseBuilder(Boolean.FALSE);
        }
    }

    private boolean sendEmail(String template, String locale, String address, String name, String url, String saveUrl) {
        Map<String, String> vm = new HashMap<>();
        vm.put(TEMPL_LOCALE, locale);
        vm.put(TEMPL_ADDRESS, address);
        vm.put(TEMPL_USERNAME, name);
        vm.put(TEMPL_URL, url);
        vm.put("data", saveUrl);
        vm.put(TEMPL_ENVIRONMENT, "dev");

        try {
            if (!haveTemplate(locale, template)) {
                addTemplateToCache(locale, template);
            }

            String content = getContent(locale, template, vm);
            String subject = getSubject(locale, template, vm);

            Message email = new MimeMessage(session);
            email.setFrom(new InternetAddress(senderAddr));
            email.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
            email.setSubject(MimeUtility.encodeText(subject, CHARSET_UTF_8, "B"));
            email.setContent(content, TEMPL_CONTETN);
            Transport.send(email);
            return Boolean.TRUE;
        } catch (IOException | MessagingException e) {
            return Boolean.FALSE;
        }
    }

    private String getPath(String locale, String templateName) {
        String pathEnd = getPathEnd(locale, templateName);
        return templateName == null ? "" : RESOURCE_PATH_PREFIX + pathEnd;
    }

    private String getPathEnd(String locale, String templateName) {
        if (locale != null && !"".equals(locale)) {
            return locale + "/" + templateName;
        }

        return templateName == null ? "" : templateName;
    }

    private boolean haveTemplate(String locale, String templateName) {
        if (contentCache == null) {
            return false;
        }
        String path = getPathEnd(locale, templateName);
        String contentTemplate = contentCache.get(path);
        return contentTemplate != null;
    }

    private String getSubstitute(Map<String, String> templateCache, String locale, String templateName,
                                 Map<String, String> vm) {
        String path = getPathEnd(locale, templateName);
        if (templateCache == null) {
            return "";
        }

        String template = templateCache.get(path);
        if (template == null) {
            return "";
        } else {
            return regSubstitute(template, vm);
        }
    }

    private String getSubject(String locale, String templateName, Map<String, String> vm) {
        return getSubstitute(subjectCache, locale, templateName, vm);
    }

    private String getContent(String locale, String templateName, Map<String, String> vm) {
        return getSubstitute(contentCache, locale, templateName, vm);
    }

    private void addTemplateToCache(String locale, String templateName) throws IOException {
        String pathEnd = getPathEnd(locale, templateName);
        String path = getPath(locale, templateName);

        String contentPath = path + TEMPL_SUF_CONTENT;
        String content = getStringFromFile(contentPath);
        if (contentCache == null) {
            contentCache = new HashMap<>();
        }

        contentCache.put(pathEnd, content);

        String subjectPath = path + TEMPL_SUF_SUBJECT;
        String subject = getStringFromFile(subjectPath);

        if (subjectCache == null) {
            subjectCache = new HashMap<>();
        }
        subjectCache.put(pathEnd, subject);
    }

    private String getStringFromFile(String path) throws IOException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(path);
        return IOUtils.toString(is, StandardCharsets.UTF_8.name());
    }

    private String regSubstitute(String template, Map<String, String> valuesMap) {
        StrSubstitutor sub = new StrSubstitutor(valuesMap);
        return sub.replace(template);
    }

}
