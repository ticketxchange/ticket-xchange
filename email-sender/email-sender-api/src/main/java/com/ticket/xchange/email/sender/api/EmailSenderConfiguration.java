package com.ticket.xchange.email.sender.api;

import com.ticket.xchange.util.JaxRsBean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailSenderConfiguration {

    @Value("${emailSenderService.address}")
    private String senderAppAddr;

    @Bean(name = "emailSenderClientProxyBean")
    public EmailSender emailSender() {
        return JaxRsBean.createClient(EmailSender.class, senderAppAddr);
    }

}
