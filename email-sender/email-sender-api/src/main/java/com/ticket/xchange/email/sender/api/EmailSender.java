package com.ticket.xchange.email.sender.api;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/emailsender")
public interface EmailSender {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/registration")
    Response register(@FormParam("locale") String locale, @FormParam("address") String address, @FormParam("name") String name, @FormParam("url") String url);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/forgottenpass")
    Response forgottenPassword(@FormParam("locale") String locale, @FormParam("address") String address, @FormParam("name") String name, @FormParam("url") String url, @FormParam("saveUrl") String saveUrl);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/nearbyevent")
    Response nearbyEvent(@FormParam("locale") String locale, @FormParam("address") String address, @FormParam("name") String name, @FormParam("url") String url, @FormParam("info") String info);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("/ticketbuy")
    Response ticketBuy(@FormParam("locale") String locale, @FormParam("address") String address, @FormParam("name") String name, @FormParam("ticketName") String ticketName, @FormParam("ticketPrice") Integer ticketPrice);

}
