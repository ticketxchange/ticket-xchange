package com.ticket.xchange.common.wallet;

import lombok.Data;

import java.util.Date;

@Data
public class WalletEntry {

    private Long id;
    private Integer balance;
    private String cardNumber;
    private String expirationDate;
    private String securityCode;

}
