package com.ticket.xchange.common.enums;

public enum UserStatus {
    NOT_ACTIVATED("Nem aktív"),
    ACTIVATED("Aktív"),
    NEW_PASSWORD_REQUESTED("Új jelszó kérés"),
    DEACTIVATED("Deaktív");

    private final String value;

    private UserStatus(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
