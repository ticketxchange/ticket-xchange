package com.ticket.xchange.common.enums;

public enum UserType {
    NORMAL_USER("Normál felhasználó"),
    ORGANIZER("Szervező"),
    ADMINISTRATOR("Adminisztrátor");

    private final String value;

    private UserType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
