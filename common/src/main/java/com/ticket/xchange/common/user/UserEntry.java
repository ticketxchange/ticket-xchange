package com.ticket.xchange.common.user;

import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.wallet.WalletEntry;
import com.ticket.xchange.common.enums.UserStatus;
import lombok.Data;
import lombok.ToString;

import java.time.Instant;

@Data
@ToString(exclude = "walletEntry")
public class UserEntry {

    private Long id;
    private String email;
    private String password;
    private Boolean activated;
    private String token;
    private String firstName;
    private String lastName;
    private String city;
    private WalletEntry walletEntry;
    private Boolean notifications;
    private UserType type;
    private Instant registrationDate;
    private UserStatus status;
    private String sessionId;

}
