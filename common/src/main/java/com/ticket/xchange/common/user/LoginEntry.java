package com.ticket.xchange.common.user;

import lombok.Data;

@Data
public class LoginEntry {

    private String email;
    private String password;

}
