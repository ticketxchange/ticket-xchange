package com.ticket.xchange.common.event;

import lombok.Data;

@Data
public class ModifyEventEntry {

    private Long id;
    private String name;
    private String date;
    private String city;
    private String description;

}
