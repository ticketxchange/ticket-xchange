package com.ticket.xchange.common.user;

import lombok.Data;

@Data
public class ModifyUserEntry {

    private Long id;
    private String firstName;
    private String lastName;
    private String city;
    private String email;
    private String type;
    private String status;
    private Boolean notifications;

}
