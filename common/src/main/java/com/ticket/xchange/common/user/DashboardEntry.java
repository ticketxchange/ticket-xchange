package com.ticket.xchange.common.user;

import lombok.Data;

@Data
public class DashboardEntry {
    private String name;
    private Integer balance;
}
