package com.ticket.xchange.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ModelStaticAttribute {
    private String attributeName;
    private Object attributeValue;
}
