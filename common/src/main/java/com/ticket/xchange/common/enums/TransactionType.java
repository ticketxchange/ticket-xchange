package com.ticket.xchange.common.enums;

public enum TransactionType {
    SALE("Eladás"),
    BUYING("Vétel"),
    PAYMENT("Fizetés"),
    DEPOSIT("Pénz befizetés");

    private final String value;

    private TransactionType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
