package com.ticket.xchange.common.user;

import lombok.Data;

@Data
public class ModifyProfileEntry {

    private String firstName;
    private String lastName;
    private String city;
    private String password;
    private Boolean notifications;

}
