package com.ticket.xchange.common.wallet;

import lombok.Data;

import java.util.Date;

@Data
public class ModifyWalletEntry {

    private Long id;
    private String cardNumber;
    private String expirationDate;
    private String securityCode;

}
