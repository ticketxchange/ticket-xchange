package com.ticket.xchange.common.transaction;

import com.ticket.xchange.common.enums.TransactionType;
import com.ticket.xchange.common.event.EventEntry;
import com.ticket.xchange.common.ticket.TicketEntry;
import com.ticket.xchange.common.user.UserEntry;
import lombok.Data;

import java.time.Instant;

@Data
public class TransactionEntry {

    private Long id;
    private EventEntry eventEntry;
    private TicketEntry ticketEntry;
    private UserEntry sellerEntry;
    private UserEntry buyerEntry;
    private Instant date;
    private TransactionType type;

}
