package com.ticket.xchange.common.user;

import lombok.Data;

@Data
public class RegistrationEntry {
    private String firstName;
    private String lastName;
    private String city;
    private String email;
    private String password;
    private String passwordConfirm;
}
