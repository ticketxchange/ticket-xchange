package com.ticket.xchange.common.auditlog;

import com.ticket.xchange.common.enums.AuditLogType;
import lombok.Data;

@Data
public class AuditLogEntry {
    
    private Long id;
    private AuditLogType type;
    private String description;
    
}
