package com.ticket.xchange.common.user;

import lombok.Data;

@Data
public class SessionEntry {
    private String userEmail;
    private String sessionId;
}
