package com.ticket.xchange.common.ticket;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ModifyTicketEntry {

    private Long id;
    private Long eventId;
    private String name;
    private Integer price;
    private String validity;
    private String description;
    private String imageUrl;

}
