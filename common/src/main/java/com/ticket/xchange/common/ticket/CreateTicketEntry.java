package com.ticket.xchange.common.ticket;

import lombok.Data;

import java.time.Instant;

@Data
public class CreateTicketEntry {

    private Long eventId;
    private String name;
    private Integer price;
    private String validity;
    private String description;
    private String imageUrl;

}
