package com.ticket.xchange.common.event;

import com.ticket.xchange.common.user.UserEntry;
import lombok.Data;

import java.time.Instant;

@Data
public class EventEntryDistance {

    private Long id;
    private UserEntry userEntry;
    private String name;
    private Instant date;
    private String city;
    private String description;
    private Double latitude;
    private Double longitude;
    private Double distance;

}
