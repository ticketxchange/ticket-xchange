package com.ticket.xchange.common.enums;

public enum TicketStatus {
    SALE("Eladó"),
    BOUGHT("Megvásárolt"),
    USED("Felhasznált"),
    DELETED("Eltávolított"),
    EXPIRED("Lejárt");

    private final String value;

    private TicketStatus(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
