package com.ticket.xchange.common.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ForgottenPasswordEntry {

    private String email;
    private String password;
    private String passwordConfirm;
    private String token;

}
