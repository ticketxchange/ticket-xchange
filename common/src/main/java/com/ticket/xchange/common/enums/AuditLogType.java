package com.ticket.xchange.common.enums;

public enum AuditLogType {
    INFORMATION("Információ"),
    WARNING("Figyelemeztetés"),
    ERROR("Hiba"),
    UNKNOWN("Ismeretlen");

    private final String value;

    private AuditLogType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
