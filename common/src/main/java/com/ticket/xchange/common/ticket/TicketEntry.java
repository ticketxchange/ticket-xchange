package com.ticket.xchange.common.ticket;

import com.ticket.xchange.common.enums.TicketStatus;
import com.ticket.xchange.common.event.EventEntry;
import com.ticket.xchange.common.user.UserEntry;
import lombok.Data;
import lombok.ToString;

import java.time.Instant;

@Data
@ToString
public class TicketEntry {

    private Long id;
    private UserEntry userEntry;
    private EventEntry eventEntry;
    private String name;
    private Integer price;
    private Instant validity;
    private String description;
    private TicketStatus status;
    private String imageUrl;

}
