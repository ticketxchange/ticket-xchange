CREATE TABLE `user` (
    `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
    `email` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL UNIQUE,
    `password` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL,
    `activated` TINYINT(1) NOT NULL DEFAULT 0,
    `token` VARCHAR(36) CHARACTER SET utf8mb4 NOT NULL,
    `first_name` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL,
    `last_name` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL,
    `city` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL,
    `wallet_id` BIGINT NOT NULL,
    `notifications` TINYINT(1) NOT NULL DEFAULT 1,
    `type` VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
    `registradion_date` TIMESTAMP NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `ticket` (
    `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL,
    `event_id` BIGINT NOT NULL,
    `name` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL,
    `price` INT NULL,
    `validity` TIMESTAMP NOT NULL,
    `description` TEXT CHARACTER SET utf8mb4,
    `status` VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
    `ticket_image_url` TEXT NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `event` (
    `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
    `user_id` BIGINT NOT NULL,
    `name` VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL,
    `date` INT NULL,
    `city` TIMESTAMP NOT NULL,
    `description` TEXT CHARACTER SET utf8mb4,
    PRIMARY KEY (`id`)
);

CREATE TABLE `transaction` (
    `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
    `event_id` BIGINT NOT NULL,
    `ticket_id` BIGINT NOT NULL,
    `seller_id` BIGINT NOT NULL,
    `buyer_id` BIGINT NOT NULL,
    `date` INT NULL,
    `type` VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `auditlog` (
    `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
    `type` VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
    `description` TEXT CHARACTER SET utf8mb4 NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `wallet` (
    `id` BIGINT NOT NULL UNIQUE AUTO_INCREMENT,
    `balance` INT NOT NULL,
    `card_number` VARCHAR(100) CHARACTER SET utf8mb4,
    `expiration_date` VARCHAR(100) CHARACTER SET utf8mb4,
    `security_code` VARCHAR(100) CHARACTER SET utf8mb4,
    PRIMARY KEY (`id`)
);