ALTER TABLE `transaction` MODIFY COLUMN `event_id` BIGINT;
ALTER TABLE `transaction` MODIFY COLUMN `buyer_id` BIGINT;
ALTER TABLE `transaction` MODIFY COLUMN `ticket_id` BIGINT;
ALTER TABLE `transaction` MODIFY COLUMN `seller_id` BIGINT;
ALTER TABLE `transaction` MODIFY COLUMN `date` TIMESTAMP NOT NULL;