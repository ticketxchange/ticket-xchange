package com.ticket.xchange.backend.service.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity getByToken(String token);
    UserEntity getByEmail(String email);
    Boolean existsByEmail(String email);

}
