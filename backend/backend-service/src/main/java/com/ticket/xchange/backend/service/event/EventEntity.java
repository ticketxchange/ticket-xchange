package com.ticket.xchange.backend.service.event;

import com.ticket.xchange.backend.service.user.UserEntity;
import com.ticket.xchange.common.event.EventEntry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(name = "event")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class EventEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "date", nullable = false)
    private Instant date;

    @NotNull
    @Column(name = "city", nullable = false)
    private String city;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "latitude", nullable = false)
    private Double latitude;

    @NotNull
    @Column(name = "longitude", nullable = false)
    private Double longitude;

    public EventEntry asEntry() {
        EventEntry eventEntry = new EventEntry();
        eventEntry.setId(getId());
        eventEntry.setName(getName());
        eventEntry.setDate(getDate());
        eventEntry.setCity(getCity());
        eventEntry.setDescription(getDescription());
        eventEntry.setLatitude(getLatitude());
        eventEntry.setLongitude(getLongitude());

        if (getUserEntity() != null) {
            eventEntry.setUserEntry(getUserEntity().asEntry());
        }

        return eventEntry;
    }

    public EventEntity(EventEntry eventEntry) {
        if (eventEntry != null) {
            this.name = eventEntry.getName();
            this.date = eventEntry.getDate();
            this.city = eventEntry.getCity();
            this.description = eventEntry.getDescription();
            this.latitude = eventEntry.getLatitude();
            this.longitude = eventEntry.getLongitude();

            if (eventEntry.getUserEntry() != null) {
                this.userEntity = new UserEntity(eventEntry.getUserEntry());
            } else {
                this.userEntity = null;
            }

            if (eventEntry.getId() != null) {
                this.id = eventEntry.getId();
            } else {
                this.id = null;
            }
        }
    }

    public EventEntity(Long id) {
        this.id = id;
    }

}
