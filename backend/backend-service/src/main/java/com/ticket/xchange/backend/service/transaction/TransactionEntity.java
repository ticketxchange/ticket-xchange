package com.ticket.xchange.backend.service.transaction;

import com.ticket.xchange.backend.service.event.EventEntity;
import com.ticket.xchange.backend.service.ticket.TicketEntity;
import com.ticket.xchange.backend.service.user.UserEntity;
import com.ticket.xchange.common.enums.TransactionType;
import com.ticket.xchange.common.transaction.TransactionEntry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(name = "transaction")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    private EventEntity eventEntity;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id")
    private TicketEntity ticketEntity;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "seller_id")
    private UserEntity sellerEntity;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buyer_id")
    private UserEntity buyerEntity;

    @NotNull
    @Column(name = "date", nullable = false)
    private Instant date;

    @NotNull
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionType type;

    public TransactionEntry asEntry() {
        TransactionEntry transactionEntry = new TransactionEntry();
        transactionEntry.setId(getId());
        transactionEntry.setDate(getDate());
        transactionEntry.setType(getType());

        if (getEventEntity() != null) {
            transactionEntry.setEventEntry(getEventEntity().asEntry());
        }

        if (getTicketEntity() != null) {
            transactionEntry.setTicketEntry(getTicketEntity().asEntry());
        }

        if (getSellerEntity() != null) {
            transactionEntry.setSellerEntry(getSellerEntity().asEntry());
        }

        if (getBuyerEntity() != null) {
            transactionEntry.setBuyerEntry(getBuyerEntity().asEntry());
        }

        return transactionEntry;
    }

    public TransactionEntity(TransactionEntry transactionEntry) {
        if (transactionEntry != null) {
            this.date = transactionEntry.getDate();
            this.type = transactionEntry.getType();

            if(transactionEntry.getEventEntry() != null) {
                this.eventEntity = new EventEntity(transactionEntry.getEventEntry().getId());
            } else {
                this.eventEntity = null;
            }

            if(transactionEntry.getTicketEntry() != null) {
                this.ticketEntity = new TicketEntity(transactionEntry.getTicketEntry().getId());
            } else {
                this.ticketEntity = null;
            }

            if(transactionEntry.getSellerEntry() != null) {
                this.sellerEntity = new UserEntity(transactionEntry.getSellerEntry().getId());
            } else {
                this.sellerEntity = null;
            }

            if(transactionEntry.getBuyerEntry() != null) {
                this.buyerEntity = new UserEntity(transactionEntry.getBuyerEntry().getId());
            } else {
                this.buyerEntity = null;
            }

            if(transactionEntry.getId() != null) {
                this.id = transactionEntry.getId();
            } else {
                this.id = null;
            }
        }
    }

    public TransactionEntity(Long id) {
        this.id = id;
    }

}
