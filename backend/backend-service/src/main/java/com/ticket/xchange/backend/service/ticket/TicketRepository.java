package com.ticket.xchange.backend.service.ticket;

import com.ticket.xchange.backend.service.event.EventEntity;
import com.ticket.xchange.backend.service.user.UserEntity;
import com.ticket.xchange.common.enums.TicketStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface TicketRepository extends JpaRepository<TicketEntity, Long> {
    List<TicketEntity> findAllByEventEntity(EventEntity eventEntity);
    List<TicketEntity> findAllByEventEntityAndStatus(EventEntity eventEntity, TicketStatus ticketStatus);
    List<TicketEntity> findAllByUserEntity(UserEntity userEntity);
    List<TicketEntity> findAllByStatus(TicketStatus ticketStatus);
}
