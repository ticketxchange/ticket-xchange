package com.ticket.xchange.backend.service.wallet;

import com.ticket.xchange.backend.api.wallet.WalletService;
import com.ticket.xchange.common.wallet.WalletEntry;
import org.springframework.stereotype.Service;

@Service("walletService")
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;

    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @Override
    public WalletEntry getById(Long id) {
        WalletEntity walletEntity = walletRepository.findById(id).orElse(null);
        if (walletEntity != null) {
            return walletEntity.asEntry();
        } else return null;
    }

    @Override
    public WalletEntry modify(WalletEntry walletEntry) {
        if (walletEntry != null) {
            walletRepository.save(new WalletEntity(walletEntry));
            return walletEntry;
        } else return null;
    }
}
