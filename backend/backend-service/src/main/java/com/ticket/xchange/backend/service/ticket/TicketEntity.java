package com.ticket.xchange.backend.service.ticket;

import com.ticket.xchange.backend.service.event.EventEntity;
import com.ticket.xchange.backend.service.user.UserEntity;
import com.ticket.xchange.common.enums.TicketStatus;
import com.ticket.xchange.common.ticket.TicketEntry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(name = "ticket")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class TicketEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    //Role ManyToOne User n - 1

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    private EventEntity eventEntity;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "price", nullable = false)
    private Integer price;

    @NotNull
    @Column(name = "validity", nullable = false)
    private Instant validity;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TicketStatus status;

    @Column(name = "ticket_image_url", nullable = false)
    private String imageUrl;

    public TicketEntry asEntry() {
        TicketEntry ticketEntry = new TicketEntry();
        ticketEntry.setId(getId());
        ticketEntry.setName(getName());
        ticketEntry.setPrice(getPrice());
        ticketEntry.setValidity(getValidity());
        ticketEntry.setDescription(getDescription());
        ticketEntry.setStatus(getStatus());
        ticketEntry.setImageUrl(getImageUrl());

        if (getUserEntity() != null) {
            ticketEntry.setUserEntry(getUserEntity().asEntry());
        }

        if (getEventEntity() != null) {
            ticketEntry.setEventEntry(getEventEntity().asEntry());
        }

        return ticketEntry;
    }

    public TicketEntity(TicketEntry ticketEntry) {
        if (ticketEntry != null) {
            this.name = ticketEntry.getName();
            this.price = ticketEntry.getPrice();
            this.validity = ticketEntry.getValidity();
            this.description = ticketEntry.getDescription();
            this.status = ticketEntry.getStatus();
            this.imageUrl = ticketEntry.getImageUrl();

            if(ticketEntry.getUserEntry() != null) {
                this.userEntity = new UserEntity(ticketEntry.getUserEntry().getId());
            } else {
                this.userEntity = null;
            }

            if(ticketEntry.getEventEntry() != null) {
                this.eventEntity = new EventEntity(ticketEntry.getEventEntry().getId());
            } else {
                this.eventEntity = null;
            }

            if (ticketEntry.getId() != null) {
                this.id = ticketEntry.getId();
            } else {
                this.id = null;
            }
        }
    }

    public TicketEntity(Long id) {
        this.id = id;
    }

}
