package com.ticket.xchange.backend.service.auditlog;

import com.ticket.xchange.common.auditlog.AuditLogEntry;
import com.ticket.xchange.common.enums.AuditLogType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "auditlog")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class AuditLogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @NotNull
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private AuditLogType type;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    public AuditLogEntry asEntry() {
        AuditLogEntry auditLogEntry = new AuditLogEntry();
        auditLogEntry.setId(getId());
        auditLogEntry.setType(getType());
        auditLogEntry.setDescription(getDescription());
        return auditLogEntry;
    }

    public AuditLogEntity(AuditLogEntry auditLogEntry) {
        if (auditLogEntry != null) {
            this.type = auditLogEntry.getType();
            this.description = auditLogEntry.getDescription();

            if (auditLogEntry.getId() != null) {
                this.id = auditLogEntry.getId();
            } else {
                this.id = null;
            }
        }
    }

    public AuditLogEntity(Long id) {
        this.id = id;
    }

}
