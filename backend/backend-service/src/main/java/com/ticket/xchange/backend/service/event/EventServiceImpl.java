package com.ticket.xchange.backend.service.event;

import com.ticket.xchange.backend.api.event.EventService;
import com.ticket.xchange.common.event.EventEntry;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("eventService")
public class EventServiceImpl implements EventService {

    private final EventRepository eventRepository;

    EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public List<EventEntry> getAll() {
        List<EventEntity> eventEntities = eventRepository.findAll();
        if (!CollectionUtils.isEmpty(eventEntities)) {
            return eventEntities.stream().map(EventEntity::asEntry).collect(Collectors.toList());
        } else return new ArrayList<>();
    }

    @Override
    public EventEntry getById(Long id) {
        EventEntity eventEntity = eventRepository.findById(id).orElse(null);
        if (eventEntity != null) {
            return eventEntity.asEntry();
        } else return null;
    }

    @Override
    public EventEntry create(EventEntry eventEntry) {
        if (eventEntry != null) {
            eventRepository.save(new EventEntity(eventEntry));
            return eventEntry;
        } else return null;
    }

    @Override
    public EventEntry modify(EventEntry eventEntry) {
        if (eventEntry != null) {
            eventRepository.save(new EventEntity(eventEntry));
            return eventEntry;
        } else return null;
    }

    @Override
    public Boolean remove(Long id) {
        EventEntity eventEntity = eventRepository.findById(id).orElse(null);
        if (eventEntity != null) {
            eventRepository.delete(eventEntity);
            return true;
        } else return null;
    }

    @Override
    public Boolean existsByName(String name) {
        EventEntity eventEntity = eventRepository.findFirstByName(name).orElse(null);
        return (eventEntity != null);
    }
}
