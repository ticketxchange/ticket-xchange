package com.ticket.xchange.backend.service;

import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.ticket.xchange.backend.service.event.EventEntity;
import com.ticket.xchange.backend.service.event.EventRepository;
import com.ticket.xchange.backend.service.user.UserEntity;
import com.ticket.xchange.backend.service.user.UserRepository;
import com.ticket.xchange.common.event.EventEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.email.sender.api.EmailSender;
import com.ticket.xchange.util.GeocodingUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class NotificationComponent {

    @Value("${frontendUrl}")
    public String frontendUrl;

    private final UserRepository userRepository;
    private final EventRepository eventRepository;

    @Autowired @Qualifier(value = "emailSender") public EmailSender emailSender;

    public NotificationComponent(UserRepository userRepository,
                                 EventRepository eventRepository) {
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
    }

    @Scheduled(fixedDelay = 1800000) //30 perc
    public void sendCloseEventEmailForUser() {
        List<UserEntry> userEntries = mapToUserList(userRepository.findAll());
        List<EventEntry> eventEntries = mapToEventList(eventRepository.findAll());

        eventEntries.forEach(eventEntry -> userEntries.forEach(userEntry -> {
            Double distance = getDistance(userEntry, eventEntry);
            if ((distance / 1000.0) < 50.0 && userEntry.getNotifications()) {
                log.info("Automatic notification sending to '" + userEntry.getEmail() + "'. Distance between the event and user is " + (distance / 1000.0) + " km.");
                emailSender.nearbyEvent("hu",
                        userEntry.getEmail().trim(),
                        userEntry.getFirstName().trim(),
                        frontendUrl + "/event?id=" + eventEntry.getId(),
                        eventEntry.getName() + " (" + (distance / 1000.0) + " km)");
            }
        }));
    }

    private List<UserEntry> mapToUserList(List<UserEntity> userEntities) {
        if (!CollectionUtils.isEmpty(userEntities)) {
            return userEntities.stream().map(UserEntity::asEntry).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private List<EventEntry> mapToEventList(List<EventEntity> eventEntities) {
        if (!CollectionUtils.isEmpty(eventEntities)) {
            return eventEntities.stream().map(EventEntity::asEntry).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    private Double getDistance(UserEntry userEntry, EventEntry eventEntry) {
        JOpenCageLatLng userLatLon = GeocodingUtils.getLatitudeAngLongitudeFromAddress(userEntry.getCity());
        return (GeocodingUtils.getDistance(userLatLon.getLat(), userLatLon.getLng(), eventEntry.getLatitude(), eventEntry.getLongitude()));
    }
}
