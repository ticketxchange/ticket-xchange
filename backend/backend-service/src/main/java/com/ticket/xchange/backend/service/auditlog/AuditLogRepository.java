package com.ticket.xchange.backend.service.auditlog;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface AuditLogRepository extends JpaRepository<AuditLogEntity, Long> {
}
