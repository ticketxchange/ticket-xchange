package com.ticket.xchange.backend.service.transaction;

import com.ticket.xchange.backend.api.transaction.TransactionService;
import com.ticket.xchange.common.transaction.TransactionEntry;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public List<TransactionEntry> getAll() {
        List<TransactionEntity> transactionEntities = transactionRepository.findAll();
        if (!CollectionUtils.isEmpty(transactionEntities)) {
            return transactionEntities.stream().map(TransactionEntity::asEntry).collect(Collectors.toList());
        } else return new ArrayList<>();
    }

    @Override
    public TransactionEntry create(TransactionEntry transactionEntry) {
        if (transactionEntry != null) {
            transactionRepository.save(new TransactionEntity(transactionEntry));
            return transactionEntry;
        } else return null;
    }
}
