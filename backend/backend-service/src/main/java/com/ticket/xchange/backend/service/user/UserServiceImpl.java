package com.ticket.xchange.backend.service.user;

import com.ticket.xchange.backend.service.wallet.WalletEntity;
import com.ticket.xchange.backend.service.wallet.WalletRepository;
import com.ticket.xchange.common.enums.UserStatus;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.user.LoginEntry;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.common.wallet.WalletEntry;
import com.ticket.xchange.util.SessionManagement;
import org.springframework.stereotype.Service;
import com.ticket.xchange.backend.api.user.UserService;
import com.ticket.xchange.util.PasswordPolicyChecker;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("userService")
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final WalletRepository walletRepository;

    UserServiceImpl(UserRepository userRepository,
                    WalletRepository walletRepository) {
        this.userRepository = userRepository;
        this.walletRepository = walletRepository;
    }

    @Override
    public UserEntry login(LoginEntry loginEntry) {
        if (loginEntry != null) {
            UserEntity userEntity = userRepository.getByEmail(loginEntry.getEmail());
            if (userEntity != null) {
                if (userEntity.getPassword().equals(loginEntry.getPassword())) {
                    resetUserToken(userEntity);
                    setSessionId(userEntity);
                    return userEntity.asEntry();
                }
            }
        }
        return null;
    }

    @Override
    public UserEntry registration(UserEntry userEntry) {
        if (userEntry != null) {
            WalletEntry walletEntry = new WalletEntry();
            walletEntry.setBalance(0);

            WalletEntity walletEntity = walletRepository.save(new WalletEntity(walletEntry));
            if (walletEntity != null) {
                userEntry.setWalletEntry(walletEntity.asEntry());
            }

            UserEntity userEntity = userRepository.save(new UserEntity(userEntry));
            if (userEntity != null) {
                return userEntity.asEntry();
            }
        }
        return null;
    }

    @Override
    public UserEntry activation(String token) {
        if (!StringUtils.isEmpty(token)) {
            UserEntity userEntity = userRepository.getByToken(token);
            if (userEntity != null) {
                if (UserStatus.NOT_ACTIVATED.equals(userEntity.getStatus())) {
                    userEntity.setActivated(true);
                    userEntity.setToken("");
                    userEntity.setStatus(UserStatus.ACTIVATED);
                    userRepository.save(userEntity);
                    return userEntity.asEntry();
                }
            }
        }
        return null;
    }

    @Override
    public Boolean isEmailUsed(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Boolean isPasswordGood(String password) {
        PasswordPolicyChecker passwordPolicyChecker = new PasswordPolicyChecker(password);
        return passwordPolicyChecker.isPasswordGood();
    }

    @Override
    public Boolean forgottenPassword(String email, String token) {
        if (!StringUtils.isEmpty(email)) {
            UserEntity userEntity = userRepository.getByEmail(email);
            if (userEntity != null) {
                if (UserStatus.NOT_ACTIVATED.equals(userEntity.getStatus()) ||
                        UserStatus.DEACTIVATED.equals(userEntity.getStatus()) ||
                        UserType.ADMINISTRATOR.equals(userEntity.getType())) {
                    return false;
                }
                userEntity.setToken(token);
                userEntity.setStatus(UserStatus.NEW_PASSWORD_REQUESTED);
                userRepository.save(userEntity);
                return true;
            }
        }
        return false;
    }

    @Override
    public UserEntry getByToken(String token) {
        if (!StringUtils.isEmpty(token)) {
            UserEntity userEntity = userRepository.getByToken(token.trim());
            if (userEntity != null) {
                return userEntity.asEntry();
            }
        }
        return null;
    }

    @Override
    public UserEntry getByEmail(String email) {
        if (!StringUtils.isEmpty(email)) {
            UserEntity userEntity = userRepository.getByEmail(email.trim());
            if (userEntity != null) {
                return userEntity.asEntry();
            }
        }
        return null;
    }

    @Override
    public UserEntry modify(UserEntry userEntry) {
        if (userEntry != null) {
            UserEntity userEntity = userRepository.save(new UserEntity(userEntry));
            if (userEntity != null) {
                return userEntity.asEntry();
            }
        }
        return null;
    }

    @Override
    public Boolean isUserDeactivated(String email) {
        UserEntity userEntity = userRepository.getByEmail(email);
        if (userEntity != null) {
            return (UserStatus.DEACTIVATED.equals(userEntity.getStatus()));
        }
        return false;
    }

    @Override
    public Boolean logout(String email) {
        UserEntity userEntity = userRepository.getByEmail(email);
        if (userEntity != null) {
            userEntity.setSessionId(null);
            userRepository.save(userEntity);
            return true;
        }
        return false;
    }

    @Override
    public List<UserEntry> getAll() {
        List<UserEntity> userEntities = userRepository.findAll();
        if (!CollectionUtils.isEmpty(userEntities)) {
            return userEntities.stream().map(UserEntity::asEntry).collect(Collectors.toList());
        } else return new ArrayList<>();
    }

    @Override
    public UserEntry getById(Long id) {
        UserEntity userEntity = userRepository.findById(id).orElse(null);
        if (userEntity != null) {
            return userEntity.asEntry();
        } else return null;
    }

    private void resetUserToken(UserEntity userEntity) {
        if (!StringUtils.isEmpty(userEntity.getToken())) {
            if (UserStatus.ACTIVATED.equals(userEntity.getStatus())) {
                userEntity.setToken("");
                userRepository.save(userEntity);
            }
        }
    }

    private void setSessionId(UserEntity userEntity) {
        userEntity.setSessionId(SessionManagement.getRandomSessionId());
        userRepository.save(userEntity);
    }
}
