package com.ticket.xchange.backend.service.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface EventRepository extends JpaRepository<EventEntity, Long> {
    Optional<EventEntity> findFirstByName(String name);
}
