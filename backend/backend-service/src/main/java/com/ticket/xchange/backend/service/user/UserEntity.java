package com.ticket.xchange.backend.service.user;

import com.ticket.xchange.common.enums.UserStatus;
import com.ticket.xchange.common.enums.UserType;
import com.ticket.xchange.common.user.UserEntry;
import com.ticket.xchange.backend.service.wallet.WalletEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;


@Entity
@Table(name = "user")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "password", nullable = false)
    private String password;

    @NotNull
    @Column(name = "activated", nullable = false)
    private Boolean activated;

    @Column(name = "token")
    private String token;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Column(name = "city", nullable = false)
    private String city;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallet_id")
    private WalletEntity walletEntity;

    @NotNull
    @Column(name = "notifications", nullable = false)
    private Boolean notifications;

    @NotNull
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserType type;

    @CreationTimestamp
    @Column(name = "registration_date")
    private Instant registrationDate;

    @NotNull
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Column(name = "session_id", nullable = false)
    private String sessionId;

    public UserEntry asEntry() {
        UserEntry userEntry = new UserEntry();
        userEntry.setId(getId());
        userEntry.setEmail(getEmail());
        userEntry.setPassword(getPassword());
        userEntry.setActivated(getActivated());
        userEntry.setToken(getToken());
        userEntry.setFirstName(getFirstName());
        userEntry.setLastName(getLastName());
        userEntry.setCity(getCity());
        userEntry.setNotifications(getNotifications());
        userEntry.setType(getType());
        userEntry.setRegistrationDate(getRegistrationDate());
        userEntry.setStatus(getStatus());
        userEntry.setSessionId(getSessionId());

        if (getWalletEntity() != null) {
            userEntry.setWalletEntry(getWalletEntity().asEntry());
        }

        return userEntry;
    }

    public UserEntity(UserEntry userEntry) {
        if (userEntry != null) {
            this.email = userEntry.getEmail();
            this.password = userEntry.getPassword();
            this.activated = userEntry.getActivated();
            this.token = userEntry.getToken();
            this.firstName = userEntry.getFirstName();
            this.lastName = userEntry.getLastName();
            this.city = userEntry.getCity();
            this.notifications = userEntry.getNotifications();
            this.registrationDate = userEntry.getRegistrationDate();
            this.type = userEntry.getType();
            this.status = userEntry.getStatus();
            this.sessionId = userEntry.getSessionId();

            if (userEntry.getWalletEntry() != null) {
                this.walletEntity = new WalletEntity(userEntry.getWalletEntry().getId());
            } else {
                this.walletEntity = null;
            }

            if (userEntry.getId() != null) {
                this.id = userEntry.getId();
            } else {
                this.id = null;
            }
        }
    }

    public UserEntity(Long id) {
        this.id = id;
    }


}
