package com.ticket.xchange.backend.service;

import com.ticket.xchange.backend.service.ticket.TicketEntity;
import com.ticket.xchange.backend.service.ticket.TicketRepository;
import com.ticket.xchange.common.enums.TicketStatus;
import com.ticket.xchange.common.ticket.TicketEntry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TicketComponent {

    private final TicketRepository ticketRepository;

    public TicketComponent(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Scheduled(fixedDelay = 900000) //15 perc
    public void updateTickets() {
        List<TicketEntry> ticketEntries = mapToTicketList(ticketRepository.findAll());

        ticketEntries.forEach(ticketEntry -> {
            Instant now = Instant.now();
            if (now.isAfter(ticketEntry.getValidity())) {
                ticketEntry.setStatus(TicketStatus.EXPIRED);
                ticketRepository.save(new TicketEntity(ticketEntry));
                log.info("Ticket (ID: " + ticketEntry.getId() + ") status set to expired.");
            }
        });
    }

    private List<TicketEntry> mapToTicketList(List<TicketEntity> ticketEntities) {
        if (!CollectionUtils.isEmpty(ticketEntities)) {
            return ticketEntities.stream().map(TicketEntity::asEntry).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
