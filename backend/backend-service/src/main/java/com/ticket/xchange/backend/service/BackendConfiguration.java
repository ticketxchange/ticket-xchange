package com.ticket.xchange.backend.service;

import com.ticket.xchange.backend.service.event.EventServiceImpl;
import com.ticket.xchange.backend.service.ticket.TicketServiceImpl;
import com.ticket.xchange.backend.service.transaction.TransactionServiceImpl;
import com.ticket.xchange.backend.service.user.UserServiceImpl;
import com.ticket.xchange.backend.service.wallet.WalletServiceImpl;
import com.ticket.xchange.email.sender.api.EmailSender;
import com.ticket.xchange.util.JaxRsBean;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.apache.cxf.endpoint.Server;

@Configuration
@ComponentScan("com.ticket.xchange.backend")
public class BackendConfiguration {

    @Value("${emailSenderAddress}")
    private String emailSenderAddress;

    @Autowired UserServiceImpl userService;
    @Autowired EventServiceImpl eventService;
    @Autowired TicketServiceImpl ticketService;
    @Autowired WalletServiceImpl walletService;
    @Autowired TransactionServiceImpl transactionService;

    @Bean(name = "cxf")
    public SpringBus bus() {
        return new SpringBus();
    }

    @Bean
    public Server userServiceServer() {
        return JaxRsBean.createServer(bus(), userService, "/user");
    }

    @Bean
    public Server eventServiceServer() {
        return JaxRsBean.createServer(bus(), eventService, "/event");
    }

    @Bean
    public Server ticketServiceServer() {
        return JaxRsBean.createServer(bus(), ticketService, "/ticket");
    }

    @Bean
    public Server walletServiceServer() {
        return JaxRsBean.createServer(bus(), walletService, "/wallet");
    }

    @Bean
    public Server transactionServiceServer() {
        return JaxRsBean.createServer(bus(), transactionService, "/transaction");
    }

    @Bean
    public EmailSender emailSender() { return JaxRsBean.createClient(EmailSender.class, emailSenderAddress); }

    @Bean
    public ServletRegistrationBean cxfServletRegistrationBean() {
        return new ServletRegistrationBean(new CXFServlet(), "/*");
    }

}
