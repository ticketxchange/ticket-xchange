package com.ticket.xchange.backend.service.wallet;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface WalletRepository extends JpaRepository<WalletEntity, Long> {
}
