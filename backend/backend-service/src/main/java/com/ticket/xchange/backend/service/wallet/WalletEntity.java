package com.ticket.xchange.backend.service.wallet;

import com.ticket.xchange.common.wallet.WalletEntry;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "wallet")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class WalletEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    @NotNull
    @Column(name = "balance", nullable = false)
    private Integer balance;

    @Column(name = "card_number", nullable = false)
    private String cardNumber;

    @Column(name = "expiration_date", nullable = false)
    private String expirationDate;

    @Column(name = "security_code", nullable = false)
    private String securityCode;

    public WalletEntry asEntry() {
        WalletEntry walletEntry = new WalletEntry();
        walletEntry.setId(getId());
        walletEntry.setBalance(getBalance());
        walletEntry.setCardNumber(getCardNumber());
        walletEntry.setExpirationDate(getExpirationDate());
        walletEntry.setSecurityCode(getSecurityCode());
        return walletEntry;
    }

    public WalletEntity(WalletEntry walletEntry) {
        if (walletEntry != null) {
            this.balance = walletEntry.getBalance();
            this.cardNumber = walletEntry.getCardNumber();
            this.expirationDate = walletEntry.getExpirationDate();
            this.securityCode = walletEntry.getSecurityCode();

            if(walletEntry.getId() != null) {
                this.id = walletEntry.getId();
            } else {
                this.id = null;
            }
        }
    }

    public WalletEntity(Long id) {
        this.id = id;
    }
}
