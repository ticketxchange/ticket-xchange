package com.ticket.xchange.backend.service.ticket;

import com.ticket.xchange.backend.api.ticket.TicketService;
import com.ticket.xchange.backend.service.event.EventEntity;
import com.ticket.xchange.backend.service.event.EventRepository;
import com.ticket.xchange.backend.service.user.UserEntity;
import com.ticket.xchange.backend.service.user.UserRepository;
import com.ticket.xchange.common.enums.TicketStatus;
import com.ticket.xchange.common.ticket.TicketEntry;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ticketService")
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final EventRepository eventRepository;
    private final UserRepository userRepository;

    public TicketServiceImpl(TicketRepository ticketRepository,
                             EventRepository eventRepository,
                             UserRepository userRepository) {
        this.ticketRepository = ticketRepository;
        this.eventRepository = eventRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<TicketEntry> getAll() {
        List<TicketEntity> ticketEntities = ticketRepository.findAll();
        if (!CollectionUtils.isEmpty(ticketEntities)) {
            return ticketEntities.stream().map(TicketEntity::asEntry).collect(Collectors.toList());
        } else return new ArrayList<>();
    }

    @Override
    public TicketEntry getById(Long id) {
        TicketEntity ticketEntity = ticketRepository.findById(id).orElse(null);
        if (ticketEntity != null) {
            return ticketEntity.asEntry();
        } else return null;
    }

    @Override
    public List<TicketEntry> listByEventId(Long eventId) {
        EventEntity eventEntity = eventRepository.findById(eventId).orElse(null);
        if (eventEntity != null) {
            List<TicketEntity> ticketEntities = ticketRepository.findAllByEventEntity(eventEntity);
            if (!CollectionUtils.isEmpty(ticketEntities)) {
                return ticketEntities.stream().map(TicketEntity::asEntry).collect(Collectors.toList());
            } else return new ArrayList<>();
        } else return new ArrayList<>();
    }

    @Override
    public List<TicketEntry> listAvaialbleByEventId(Long eventId, TicketStatus ticketStatus) {
        EventEntity eventEntity = eventRepository.findById(eventId).orElse(null);
        if (eventEntity != null) {
            List<TicketEntity> ticketEntities = ticketRepository.findAllByEventEntityAndStatus(eventEntity, ticketStatus);
            if (!CollectionUtils.isEmpty(ticketEntities)) {
                return ticketEntities.stream().map(TicketEntity::asEntry).collect(Collectors.toList());
            } else return new ArrayList<>();
        } else return new ArrayList<>();
    }

    @Override
    public List<TicketEntry> listByUserId(Long userId) {
        UserEntity userEntity = userRepository.findById(userId).orElse(null);
        if (userEntity != null) {
            List<TicketEntity> ticketEntities = ticketRepository.findAllByUserEntity(userEntity);
            if (!CollectionUtils.isEmpty(ticketEntities)) {
                return ticketEntities.stream().map(TicketEntity::asEntry).collect(Collectors.toList());
            } else return new ArrayList<>();
        } else return new ArrayList<>();
    }

    @Override
    public List<TicketEntry> listByStatus(TicketStatus status) {
        if (status != null) {
            List<TicketEntity> ticketEntities = ticketRepository.findAllByStatus(status);
            if (!CollectionUtils.isEmpty(ticketEntities)) {
                return ticketEntities.stream().map(TicketEntity::asEntry).collect(Collectors.toList());
            } else return new ArrayList<>();
        } else return new ArrayList<>();
    }

    @Override
    public TicketEntry create(TicketEntry ticketEntry) {
        if (ticketEntry != null) {
            ticketRepository.save(new TicketEntity(ticketEntry));
            return ticketEntry;
        } else return null;
    }

    @Override
    public TicketEntry modify(TicketEntry ticketEntry) {
        if (ticketEntry != null) {
            ticketRepository.save(new TicketEntity(ticketEntry));
            return ticketEntry;
        } else return null;
    }

    @Override
    public Boolean remove(Long id) {
        TicketEntity ticketEntity = ticketRepository.findById(id).orElse(null);
        if (ticketEntity != null) {
            ticketRepository.delete(ticketEntity);
            return true;
        } else return null;
    }
}
