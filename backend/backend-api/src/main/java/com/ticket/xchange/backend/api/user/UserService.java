package com.ticket.xchange.backend.api.user;

import com.ticket.xchange.common.user.LoginEntry;
import com.ticket.xchange.common.user.UserEntry;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/user")
public interface UserService {

    @POST
    @Path("/authentication")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry login(LoginEntry loginEntry);

    @POST
    @Path("/registration")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry registration(UserEntry userEntry);

    @GET
    @Path("/activation")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry activation(@QueryParam("token") String token);

    @GET
    @Path("/emailused")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean isEmailUsed(@QueryParam("email") String email);

    @GET
    @Path("/passwordgood")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean isPasswordGood(@QueryParam("password") String password);

    @GET
    @Path("/forgottensend")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean forgottenPassword(@QueryParam("email") String email, @QueryParam("token") String token);

    @GET
    @Path("/getbytoken")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry getByToken(@QueryParam("token") String token);

    @GET
    @Path("/getbyemailn")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry getByEmail(@QueryParam("email") String email);

    @POST
    @Path("/modify")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry modify(UserEntry userEntry);

    @GET
    @Path("/userdeactivated")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean isUserDeactivated(@QueryParam("email") String email);

    @GET
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean logout(@QueryParam("email") String email);

    @GET
    @Path("/getall")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<UserEntry> getAll();

    @GET
    @Path("/getbyid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserEntry getById(@QueryParam("id") Long id);
}