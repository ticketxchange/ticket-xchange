package com.ticket.xchange.backend.api.wallet;

import com.ticket.xchange.common.wallet.WalletEntry;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/ticket")
public interface WalletService {

    @GET
    @Path("/getbyid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WalletEntry getById(@QueryParam("id") Long id);

    @POST
    @Path("/modify")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WalletEntry modify(WalletEntry walletEntry);

}
