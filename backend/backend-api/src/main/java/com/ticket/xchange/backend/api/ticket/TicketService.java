package com.ticket.xchange.backend.api.ticket;

import com.ticket.xchange.common.enums.TicketStatus;
import com.ticket.xchange.common.ticket.TicketEntry;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/ticket")
public interface TicketService {

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<TicketEntry> getAll();

    @GET
    @Path("/getbyid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TicketEntry getById(@QueryParam("id") Long id);

    @GET
    @Path("/getbyeventid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<TicketEntry> listByEventId(@QueryParam("eventId") Long eventId);

    @GET
    @Path("/getbyeventidandstatus")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<TicketEntry> listAvaialbleByEventId(@QueryParam("eventId") Long eventId, @QueryParam("status") TicketStatus status);

    @GET
    @Path("/getbyuserid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<TicketEntry> listByUserId(@QueryParam("userId") Long userId);

    @GET
    @Path("/getbystatus")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<TicketEntry> listByStatus(@QueryParam("status") TicketStatus status);

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TicketEntry create(TicketEntry eventEntry);

    @POST
    @Path("/modify")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TicketEntry modify(TicketEntry eventEntry);

    @POST
    @Path("/remove")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean remove(@QueryParam("id") Long id);

}
