package com.ticket.xchange.backend.api.event;

import com.ticket.xchange.common.event.EventEntry;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/event")
public interface EventService {

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<EventEntry> getAll();

    @GET
    @Path("/getbyid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    EventEntry getById(@QueryParam("id") Long id);

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    EventEntry create(EventEntry eventEntry);

    @POST
    @Path("/modify")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    EventEntry modify(EventEntry eventEntry);

    @POST
    @Path("/remove")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean remove(@QueryParam("id") Long id);

    @GET
    @Path("/existsbyname")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Boolean existsByName(@QueryParam("name") String name);
}
