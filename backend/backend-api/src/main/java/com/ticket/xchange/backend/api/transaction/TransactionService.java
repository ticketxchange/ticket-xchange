package com.ticket.xchange.backend.api.transaction;

import com.ticket.xchange.common.transaction.TransactionEntry;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/transaction")
public interface TransactionService {

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<TransactionEntry> getAll();

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TransactionEntry create(TransactionEntry eventEntry);

}
