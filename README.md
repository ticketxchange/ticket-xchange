# README #

### Ticket XChange ###

#### Információk ####

* A projekt buildeléséhez használjuk az `mvn clean install` parancsot.
* Indításhoz írjuk be a `docker-compose -f docker/docker-compose.yml up -d` parancsot. Ezzel a Docker service-on keresztül egy virtuális gépen _konténerben_ fog futni az alaklamzás.

#### Adatbázis beállítása ####

1. Az adatbázis beállításához meg kell nyitni a fő `pom.xml` fájlt. (A projekt gyökerében található)
2. A fájlban található `datasource` és `liquibase` tag-eknél az elérést írjátok át, hogy a saját MySQL szerveretekre mutasson
<br>_Alapértelmezett esetben a compose hoz létre egy adatbázist, amihez az alap adatok megfelelőek_